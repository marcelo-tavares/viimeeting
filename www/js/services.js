App.service('Dados', function ($firebase, $firebaseArray) {
    var rootDados = 'dados';
    var rootDadosSandbox = 'dadosDev'
    var urlDados = 'https://vii-meeting-scepd-dbac7.firebaseio.com/' + rootDados + '/';

    return {
        
        getRoot: function (sandbox) {
            var root = rootDados;
            if (sandbox)
                root = rootDadosSandbox;
            return root;
        },


        getUrlDados: function () { 
            return urlDados;
        },

        getRefDados: function () {
            try {
               ref = firebase.database().ref();
            }
            catch (err) {
                console.log(err.message);
            }
            finally {
                return ref;
            }
        },

        getRefDadosFilho: function (filho) {
            try {
                ref = firebase.database().ref(rootDados +'/' + filho);
            }
            catch (err) {
                console.log(err.message);
            }
            finally {
                return ref;
            }
        },

        getRefDadosFilho: function (filho, sandbox) {
            var root = rootDados;
            if (sandbox)
                root = rootDadosSandbox;
            try {
                ref = firebase.database().ref(root +'/' + filho);
            }
            catch (err) {
                console.log(err.message);
            }
            finally {
                return ref;
            }
        },


        getUrlDadosFilho: function (filho) {
            try {
                url = urlDados + filho + '.json';
            }
            catch (err) {
                console.log(err.message);
                url = "";
            }
            finally {
                return url;
            }
        },

        getUrlDadosFilhoId: function (filho, id) {
            try {
                url = urlDados + filho + '/' + id + '.json';
            }
            catch (err) {
                console.log(err.message);
                url = "";
            }
            finally {
                return url;
            }
        },

        toArray: function (ref, filho) {
            try {
                itens = $firebaseArray(ref.child(filho));
            }
            catch (err) {
                console.log(err.message);
                itens = [];
            }
            finally {
                return itens;
            }
        },

        toArrayFiltro: function (ref, filho, campo, valor) {
            try {
                itens = $firebaseArray(ref.child(filho).orderByChild(campo).equalTo(valor));
            }
            catch (err) {
                console.log(err.message);
                itens = [];
            }
            finally {
                return itens
            }

        },

        toArrayFiltroOp: function (ref, filho, campo, operacao, valor) {
            try {
                switch (operacao) {
                    case "igual":
                        itens = $firebaseArray(ref.child(filho).orderByChild(campo).equalTo(valor));
                        break;
                }

            }
            catch (err) {
                console.log(err.message);
            }
            finally {
                return itens
            }

        },
        
        encontraIndex: function (array, hash) {
            var index = array.findIndex(
            function (currentValue) {
                return currentValue.$id === hash;
            }
            );
            return index;
        },

        encontraElemento: function (array, hash) {
            var index = array.findIndex(
            function (currentValue) {
                return currentValue.$id === hash;
            }
            );
            return array[index];
        }

    }
})