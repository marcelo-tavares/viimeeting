var App = angular.module('appInscricoes', ['ui.router', 'firebase', 'ngSanitize', 'ngDialog']);

App.config(['$stateProvider', '$urlRouterProvider', roteador]);

// ---------------------------------------------------
function roteador($stateProvider, $urlRouterProvider){

    var appState = { 
        name: 'app',
        abstract: true,
        url: '/app',
        controller: 'appCtrl',
        templateUrl: 'templates/app.html'};
    
    var signUpState = {
        name: 'app.signup', 
        url: '/signup', 
        templateUrl: 'templates/signup.html', 
        controller: 'signUpCtrl'};
    
    var signInState = {
        name: 'app.signin', 
        url: '/signin', 
        templateUrl: 'templates/signin.html', 
        controller: 'signInCtrl'};

    var myMeetingState = {
        name: 'app.myMeeting', 
        url: '/myMeeting', 
        abstract: true,
        templateUrl: 'templates/myMeeting.html', 
        controller: 'myMeetingCtrl'};
    
    var perfilState = {
        name: 'app.myMeeting.perfil',
        url: '/perfil',
        templateUrl: 'templates/perfil.html',
        controller: 'perfilCtrl'};

    var individualState = {
        name: 'app.myMeeting.individual', 
        url: '/individual', 
        templateUrl: 'templates/individual.html', 
        controller: 'individualCtrl'};

    var gruposAdmState = {
        name: 'app.myMeeting.gruposAdm',
        url: '/gruposAdm',
        templateUrl: 'templates/gruposAdm.html',
        controller: 'gruposAdmCtrl'};

    var inscricoesAdmState = {
        name: 'app.myMeeting.inscricoesAdm',
        url: '/inscricoesAdm',
        templateUrl: 'templates/inscricoesAdm.html',
        controller: 'inscricoesAdmCtrl'};

    var inscricaoState = {
        name: 'app.myMeeting.inscricao',
        url: '/inscricao',
        abstract: true,
        template: '<ui-view/>',
        controller: 'inscricaoCtrl'};

    var meusGruposState = {
        name: 'app.myMeeting.inscricao.meusGrupos',
        url: '/meusGrupos',
        templateUrl: 'templates/meusGrupos.html',
        controller: 'meusGruposCtrl'};

    var minhaInscricaoState = {
        name: 'app.myMeeting.inscricao.minhaInscricao',
        url: '/minhaInscricao',
        templateUrl: 'templates/minhaInscricao.html',
        controller: 'minhaInscricaoCtrl'};

    var pagamentoState = {
        name: 'app.myMeeting.inscricao.pagamento',
        url: '/pagamento',
        abstract: true,
        template: '<ui-view/>',
        controller: 'pagamentoCtrl'};

    var pagamentoSessionState = {
        name: 'app.myMeeting.inscricao.pagamento.session',
        url: '/session',
        templateUrl: 'templates/pagamento/pagamentoSession.php',
        controller: 'pagamentoSessionCtrl'};

    var pagamentoSessionSandboxState = {
        name: 'app.myMeeting.inscricao.pagamento.sessionSandbox',
        url: '/sessionSandbox',
        templateUrl: 'templates/pagamento/pagamentoSessionSandbox.php',
        controller: 'pagamentoSessionCtrl'};

    var pagamentoMeiosState = {
        name: 'app.myMeeting.inscricao.pagamento.meios',
        url: '/meios',
        templateUrl: 'templates/pagamento/pagamentoMeios.html',
        controller: 'pagamentoMeiosCtrl'};

    var pagamentoDadosBoletoState = {
        name: 'app.myMeeting.inscricao.pagamento.dadosBoleto',
        url: '/pagamentoDadosBoleto',
        templateUrl: 'templates/pagamento/pagamentoDadosBoleto.html',
        controller: 'pagamentoDadosBoletoCtrl'};

    var pagamentoImprimirBoletoState = {
        name: 'app.myMeeting.inscricao.pagamento.imprimirBoleto',
        url: '/pagamentoImprimirBoleto',
        templateUrl: 'templates/pagamento/pagamentoImprimirBoleto.html',
        controller: 'pagamentoImprimirBoletoCtrl'};

    var pagamentoDadosCreditoState = {
        name: 'app.myMeeting.inscricao.pagamento.dadosCredito',
        url: '/pagamentoDadosCredito',
        templateUrl: 'templates/pagamento/pagamentoDadosCredito.html',
        controller: 'pagamentoDadosCreditoCtrl'};

    var pagamentoDadosDebitoState = {
        name: 'app.myMeeting.inscricao.pagamento.dadosDebito',
        url: '/pagamentoDadosDebito',
        templateUrl: 'templates/pagamento/pagamentoDadosDebito.html',
        controller: 'pagamentoDadosDebitoCtrl'};

    var dadosParticipantesState = {
        name: 'app.dadosParticipantes',
        url: '/dadosParticipantes',
        templateUrl: 'templates/dadosParticipantes.html',
        controller: 'dadosParticipantesCtrl'};

    $urlRouterProvider.otherwise('/app/signin');

    $stateProvider
        .state(appState)
        .state(signUpState)
        .state(signInState)
        .state(myMeetingState)
        .state(perfilState)
        .state(inscricaoState)
        .state(minhaInscricaoState)
        .state(meusGruposState)
        .state(inscricoesAdmState)
        .state(pagamentoState)
        .state(pagamentoSessionState)
        .state(pagamentoSessionSandboxState)
        .state(pagamentoMeiosState)
        .state(pagamentoDadosBoletoState)
        .state(pagamentoImprimirBoletoState)
        .state(pagamentoDadosCreditoState)
        .state(pagamentoDadosDebitoState)
        .state(individualState)
        .state(gruposAdmState)
        .state(dadosParticipantesState);
    
}
// ---------------------------------------------------

App.run(function($rootScope){

    $rootScope.$watch('$viewcontentLoaded',
            function(event, viewConfig){ 

        });

});