App
.factory('Inscricao', InscricaoFactory);
                       
function InscricaoFactory($http, $firebase, $firebaseArray, Dados) {

   return {
        //--- JSON ---
        jsonParticipante: function (id, uid, nome, cpf, email, telefone, endereco, categoria, cro, 
                                     instituicao, pago, dataInscricao, dataPagamento, grupo, emailEnviado, transacao) {
            return {
                id: id,
                uid:uid,
                nome: nome,
                cpf: cpf,
                email: email,
                telefone: telefone,
                endereco: endereco,
                categoria: categoria,
                cro: cro,
                instituicao: instituicao,
                pago: pago,
                dataInscricao: dataInscricao,
                dataPagamento: dataPagamento,
                grupo: grupo,
                emailEnviado: emailEnviado,
                transaction: transacao
            };
        },
        
        jsonEndereco: function (cep, tipoLogradouro, logradouro, numero, complemento, bairro, cidade, estado, logradouroCompleto) {
            return {
                cep: cep,
                tipoLogradouro: tipoLogradouro, 
                logradouro: logradouro, 
                numero: numero, 
                complemento: complemento, 
                bairro: bairro, 
                cidade: cidade, 
                estado:estado,
                logradouroCompleto: logradouroCompleto
            };
        },
        
        jsonInscricao: function (currency, itemId, descricao, valor) {
            return {
                currency: currency,
                itemId: itemId,
                descricao: descricao,
                valor: valor
            };
        },
        
        jsonGrupo: function(nome, administrador, botao) {
            return {
                nome: nome,
                administrador: administrador,
                botaoPgto: botao
            };
        },

        jsonCartaoCredito: function (nome, cpf, numero, cvv, mes, ano, nascimento, telefone, parcelas) {
            return {
                nome: nome,
                cpf: cpf,
                numero: numero,
                cvv: cvv,
                mes: mes,
                ano: ano,
                nascimento: nascimento,
                telefone: telefone,
                parcelas: parcelas
            }
        },

        jsonTransacaoPagtoEspecie: function (id, fbid, valor, data, metodo) {
            return {
                cancellationSource: "",
                code: "0000",
                date: data,
                discountAmount: "0.00",
                extraAmount: "0.00",
                feeAmount : "0.00",
                gatewaySystem : "",
                grossAmount : valor,
                installmentCount: "1",
                itemCount : "1",
                items : {
                    item : {
                        amount : valor,
                        description : "Meeting SCEPD - Inscricao Nº" + id,
                        id : "01",
                        quantity : "1"
                    }
                },
                lastEventDate : data,
                netAmount : valor,
                paymentLink : "",
                paymentMethod : {
                    code : metodo.toString(),
                    type : "8"
                },
                reference : fbid,
                sender : "",
                shipping : "",
                status : "3",
                type : "1"
                }

        },
        
        // --- VALIDACPF ---
        validaCPF: function (cpf) {
            var numeros, digitos, soma, i, resultado, digitos_iguais;
            
            cpf = cpf.replace(/\D/g, '');
            
            digitos_iguais = 1;
            if (cpf.length < 11)
                  return false;
            for (i = 0; i < cpf.length - 1; i++)
                  if (cpf.charAt(i) != cpf.charAt(i + 1))
                        {
                        digitos_iguais = 0;
                        break;
                        }
            if (!digitos_iguais)
                  {
                  numeros = cpf.substring(0,9);
                  digitos = cpf.substring(9);
                  soma = 0;
                  for (i = 10; i > 1; i--)
                        soma += numeros.charAt(10 - i) * i;
                  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                  if (resultado != digitos.charAt(0))
                        return false;
                  numeros = cpf.substring(0,10);
                  soma = 0;
                  for (i = 11; i > 1; i--)
                        soma += numeros.charAt(11 - i) * i;
                  resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                  if (resultado != digitos.charAt(1))
                        return false;
                  return true;
                  }
            else
                return false;
        }
    }
}
