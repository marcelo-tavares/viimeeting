App

.factory('Misc', miscFactory);

function miscFactory($rootScope, $sce){

    // Changes XML to JSON
    function xmlToJson(xml) {
        // Create the return object
        var obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
            obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for(var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof(obj[nodeName]) == "undefined") {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
        return obj;
    };

    function LimpaTagText(json) {
        json.transaction.code = json.transaction.code["#text"];
        json.transaction.date = json.transaction.date["#text"];
        json.transaction.discountAmount = json.transaction.discountAmount["#text"];
        json.transaction.extraAmount = json.transaction.extraAmount["#text"];
        json.transaction.feeAmount = json.transaction.feeAmount["#text"];
        json.transaction.grossAmount = json.transaction.grossAmount["#text"];
        json.transaction.installmentCount = json.transaction.installmentCount["#text"];
        json.transaction.itemCount = json.transaction.itemCount["#text"];
        json.transaction.lastEventDate = json.transaction.lastEventDate["#text"];
        json.transaction.netAmount = json.transaction.netAmount["#text"];
        json.transaction.paymentMethod.code = json.transaction.paymentMethod.code["#text"];
        json.transaction.paymentMethod.type = json.transaction.paymentMethod.type["#text"];
        json.transaction.reference = json.transaction.reference["#text"];
        json.transaction.sender.email = json.transaction.sender.email["#text"];
        json.transaction.sender.name = json.transaction.sender.name["#text"];
        json.transaction.sender.phone.areaCode = json.transaction.sender.phone.areaCode["#text"];
        json.transaction.sender.phone.number = json.transaction.sender.phone.number["#text"];
        json.transaction.shipping.address.city = json.transaction.shipping.address.city["#text"];

        if (json.transaction.paymentLink != undefined)
            json.transaction.paymentLink = json.transaction.paymentLink["#text"];
        else
            json.transaction.paymentLink = "";

        if (json.transaction.shipping.address.complement["#text"] != undefined)
            json.transaction.shipping.address.complement = json.transaction.shipping.address.complement["#text"];
        else
            json.transaction.shipping.address.complement = "";

        if (json.transaction.cancellationSource != undefined)
            json.transaction.cancellationSource = json.transaction.cancellationSource["#text"];
        else
            json.transaction.cancellationSource = "";

        json.transaction.shipping.address.country = json.transaction.shipping.address.country["#text"];
        json.transaction.shipping.address.district = json.transaction.shipping.address.district["#text"];
        json.transaction.shipping.address.number = json.transaction.shipping.address.number["#text"];
        json.transaction.shipping.address.postalCode = json.transaction.shipping.address.postalCode["#text"];
        json.transaction.shipping.address.state = json.transaction.shipping.address.state["#text"];
        json.transaction.shipping.address.street = json.transaction.shipping.address.street["#text"];
        json.transaction.shipping.cost = json.transaction.shipping.cost["#text"];
        json.transaction.shipping.type = json.transaction.shipping.type["#text"];
        json.transaction.status = json.transaction.status["#text"];
        json.transaction.type = json.transaction.type["#text"];

        json.transaction.items = limpaTextJsonItens(json.transaction.items, Number(json.transaction.itemCount))

        if (json.transaction.gatewaySystem != undefined)
            json.transaction.gatewaySystem = limpaTextJsonGatewaySystem(json.transaction.gatewaySystem)
        else
            json.transaction.gatewaySystem = "";

        return json;
    };

    // Retirar tags #text do json (Itens)
    function  limpaTextJsonItens (items, qtItems) {
        if (qtItems == 1){
            items.item.amount = items.item.amount["#text"];
            items.item.description = items.item.description["#text"];
            items.item.id = items.item.id["#text"];
            items.item.quantity = items.item.quantity["#text"];
        }
        else {
            //            for (i=0; i<qtItems; i++) {
            //                items[i].item.amount = items[i].item.amount["#text"];
            //                items[i].item.description = items[i].item.description["#text"];
            //                items[i].item.id = items[i].item.id["#text"];
            //                items[i].item.quantity = items[i].item.quantity["#text"];
            //            }
        }
        return items;
    }

    // Retirar tags #text do json (Itens)
    function  limpaTextJsonGatewaySystem (gatewaySystem) {
        return {
            acquirerName: gatewaySystem.acquirerName["#text"],
            authorizationCode: gatewaySystem.authorizationCode["#text"],
            establishmentCode: gatewaySystem.establishmentCode["#text"],
            nsu: gatewaySystem.nsu["#text"],
            tid: gatewaySystem.tid["#text"],
            type: gatewaySystem.type["#text"]
            }
    }

    return{
        validaExpReg: function(valor, expressao) {
            var expReg = new RegExp(expressao);
            return expReg.test(valor);
        },
        
        enviaEmail: function(dadosEmail){
            var paginaSendEmail = document.getElementById('se').contentDocument;
            var formulario = paginaSendEmail.getElementById('formSendEmail');
        
            paginaSendEmail.getElementById('nomeremetente').value = dadosEmail.nomeRemetente;
            paginaSendEmail.getElementById('emailremetente').value = dadosEmail.emailRemetente;
            paginaSendEmail.getElementById('emaildestinatario').value = dadosEmail.emailDestinatario;
            paginaSendEmail.getElementById('comcopia').value = dadosEmail.emailCc;
            paginaSendEmail.getElementById('comcopiaoculta').value = dadosEmail.emailCco;
            paginaSendEmail.getElementById('assunto').value = dadosEmail.assunto;
            paginaSendEmail.getElementById('mensagem').value = dadosEmail.mensagem;
        
            formulario.submit();
        },
        
        getCategoria: function (id) {
            switch (id) {
                case 1:
                    descricao = "Sócio SCEPD";
                    break;
                case 2:
                    descricao = "Sócio SBRO";
                    break;
                case 3:
                    descricao = "Profissional (CD)";
                    break;
                case 4:
                    descricao = "Acadêmico";
                    break;
                case 5:
                    descricao = "Profissional (TPD)";
                    break;
                case 93:
                    descricao = "Cortesia CD";
                    break;
                case 95:
                    descricao = "Cortesia TPD";
                    break;
                case 92:
                    descricao = "Cortesia SBRO";
                    break;
                case 94:
                    descricao = "Cortesia Acad.";
                    break;
                default:
                    descricao = "Categoria não identificada!";
                    break;
            }

            return descricao;
        },
    
        getComplemento: function(complemento) {
        var retorno = "";
        if (complemento)
            retorno =  " - " + complemento;
        return retorno;
        },
    
        getTipoInscricao: function(grupo) {
        var retorno = "Individual";
        if (grupo != 0)
            retorno =  "Grupo";
        return retorno;
        },

        // Formata Data para dd/mm/aaaa
        getDataFormatada: function(data) {
            var dataPagamento = new Date(data);

            // Dia
            d = '0' + dataPagamento.getDate();
            d = d.substr(d.length-2,2);

            // Mês
            m = '0' + (dataPagamento.getMonth()+1);
            m = m.substr(m.length-2,2);

            // Ano
            a = dataPagamento.getFullYear()

            return d + '/' + m + '/' + a
        },

        //getStatusTransacao
        getMetodoTransacao: function(metodo, parcelas) {
            var metodoPagto = "Método não identificado";
            switch (metodo) {
                case "1":
                    metodoPagto = "Cartão de Crédito (" + parcelas + "x)";
                    break;
                case "2":
                    metodoPagto = "Boleto Bancário";
                    break;
                case "3":
                    metodoPagto = "Débito Online";
                    break;
                case "4":
                    metodoPagto = "Saldo PagSeguro";
                    break;
                case "8":
                    metodoPagto = "Dinheiro / Cheque";
                    break;
            }
            return metodoPagto;
        },

        //getStatusTransacao
        getStatusTransacao: function(status) {
            var situacao = "Situação não identificada";
            switch (status) {
                case "1":
                    situacao = "Aguardando pagamento";
                    break;
                case "2":
                    situacao = "Em análise";
                    break;
                case "3":
                    situacao = "Paga";
                    break;
                case "4":
                    situacao = "Disponível";
                    break;
                case "5":
                    situacao = "Em disputa";
                    break;
                case "6":
                    situacao = "Devolvida";
                    break;
                case "7":
                    situacao = "Cancelada";
                    break;
                case "8":
                    situacao = "Debitado";
                    break;
                case "9":
                    situacao = "Retenção temporária";
                    break;
            }
            return situacao;
        },

        // Formata valor da transação
        getValorTransacao: function (valor, metodo) {
            var valorReais = "";
            if (valor != undefined) {
                valorReais = "R$" + valor.replace(".", ",");
                if (metodo == "2") {
                    valorReais += " (+ R$1,00 de taxa de boleto cobrado pelo PagSeguro)"
                }
            }
            return valorReais;
        },

        formataValorMonetario: function (valor) {
            valor = valor.toString().replace(/\D/g,"");
            valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
            valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
            valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
            return valor
        },

        formataIdInscricao: function(id){
            var strId = '000' + id;
            return strId.substr(strId.length-3,3);
        },

        buscaValorInscricao: function(categoria) {
            var pacote;
            var pacotes = [];
            var valor = [];

             for (var i=0;i<6;i++) {
                valor[i] = [];
             }

            pacotes[0] = new Date(2016,11,15,23,59,59,0);
            pacotes[1] = new Date(2017,0,15,23,59,59,0);
            pacotes[2] = new Date(2017,1,15,23,59,59,0);
            pacotes[3] = new Date(2017,2,15,23,59,59,0);
            pacotes[4] = new Date(2017,2,19,23,59,59,0);

            // Sócio SCEPD
            valor[0][0] = "0.00";     // Até 15/12/2016
            valor[0][1] = "0.00";
            valor[0][2] = "0.00";
            valor[0][3] = "0.00";
            valor[0][4] = "0.00";

            // Sócio SBRO
            valor[1][0] = "400.00";     // Até 15/12/2016
            valor[1][1] = "400.00";     // Até 15/01/2017
            valor[1][2] = "400.00";     // Até 15/02/2017
            valor[1][3] = "400.00";     // Até 15/03/2017
            valor[1][4] = "400.00";     // Após 15/03/2017

            // Profissional CD
            valor[2][0] = "600.00";     // Até 15/12/2016
            valor[2][1] = "600.00";     // Até 15/01/2017
            valor[2][2] = "600.00";     // Até 15/02/2017
            valor[2][3] = "600.00";     // Até 15/03/2017
            valor[2][4] = "600.00";     // Após 15/03/2017

            // Acadêmico
            valor[3][0] = "300.00";     // Até 15/12/2016
            valor[3][1] = "300.00";     // Até 15/01/2017
            valor[3][2] = "300.00";     // Até 15/02/2017
            valor[3][3] = "300.00";     // Até 15/03/2017
            valor[3][4] = "300.00";     // Após 15/03/2017

            // Profissional TPD
            valor[4][0] = "200.00";     // Até 15/12/2016
            valor[4][1] = "200.00";     // Até 15/01/2017
            valor[4][2] = "200.00";     // Até 15/02/2017
            valor[4][3] = "200.00";     // Até 15/03/2017
            valor[4][4] = "300.00";     // Após 15/03/2017

            // Dev
            valor[5][0] = "5.00";     // Até 15/12/2016
            valor[5][1] = "5.00";     // Até 15/01/2017
            valor[5][2] = "5.00";     // Até 15/02/2017
            valor[5][3] = "5.00";     // Até 15/03/2017
            valor[5][4] = "5.00";     // Após 15/03/2017


            var hoje = new Date();

            // Qual o pacote de hoje?

            for (var i=0; i<pacotes.length; i++)
                if (hoje < pacotes[i]) {
                    pacote = i;
                    break;
                }

            return valor[categoria-1][pacote];
        }, //buscaValorInscricao

        buscaValorInscricaoGrupo: function(categoria) {
            var pacote;
            var pacotes = [];
            var valor = [];

             for (var i=0;i<6;i++) {
                valor[i] = [];
             }

            pacotes[0] = new Date(2016,11,15,23,59,59,0);
            pacotes[1] = new Date(2017,0,15,23,59,59,0);
            pacotes[2] = new Date(2017,1,15,23,59,59,0);
            pacotes[3] = new Date(2017,2,15,23,59,59,0);
            pacotes[4] = new Date(2017,2,19,23,59,59,0);

            // Sócio SCEPD
            valor[0][0] = "0.00";
            valor[0][1] = "0.00";
            valor[0][2] = "0.00";
            valor[0][3] = "0.00";
            valor[0][4] = "0.00";

            // Sócio SBRO
            valor[1][0] = "360.00";     // Até 15/12/2016
            valor[1][1] = "360.00";     // Até 15/01/2017
            valor[1][2] = "360.00";     // Até 15/02/2017
            valor[1][3] = "360.00";     // Até 15/03/2017
            valor[1][4] = "360.00";     // Até 15/03/2017

            // Profissional CD
            valor[2][0] = "540.00";     // Até 15/12/2016
            valor[2][1] = "540.00";     // Até 15/01/2017
            valor[2][2] = "540.00";     // Até 15/02/2017
            valor[2][3] = "540.00";     // Até 15/03/2017
            valor[2][4] = "540.00";     // Após 15/03/2017

            // Acadêmico
            valor[3][0] = "270.00";     // Até 15/12/2016
            valor[3][1] = "270.00";     // Até 15/01/2017
            valor[3][2] = "270.00";     // Até 15/02/2017
            valor[3][3] = "270.00";     // Até 15/03/2017
            valor[3][4] = "270.00";     // Após 15/03/2017

            // Profissional TPD
            valor[4][0] = "180.00";     // Até 15/12/2016
            valor[4][1] = "180.00";     // Até 15/01/2017
            valor[4][2] = "180.00";     // Até 15/02/2017
            valor[4][3] = "180.00";     // Até 15/03/2017
            valor[4][4] = "270.00";     // Após 15/03/2017

            // Dev
            valor[5][0] = "5.00";     // Até 15/12/2016
            valor[5][1] = "5.00";     // Até 15/01/2017
            valor[5][2] = "5.00";     // Até 15/02/2017
            valor[5][3] = "5.00";     // Até 15/03/2017
            valor[5][4] = "5.00";     // Após 15/03/2017


            var hoje = new Date();

            // Qual o pacote de hoje?

            for (var i=0; i<pacotes.length; i++)
                if (hoje < pacotes[i]) {
                    pacote = i;
                    break;
                }

            return valor[categoria-1][pacote];
        }, //buscaValorInscricaoGrupo

        listaErros: function (erros){
            var strErros = "<ul>";
            for (erro in erros){
                strErros += "<li>" + erros[erro] + "</li>"
            }
            strErros += "</ul>";
            return strErros;
        },

        modalAlerta: function (msgAlertaLinha1, msgAlertaLinha2, titulo, tipo) {
//             $rootScope.msgAlertaLinha1 = $sce.trustAsHtml(msgAlertaLinha1);
//             $rootScope.msgAlertaLinha2 = $sce.trustAsHtml(msgAlertaLinha2);
             $rootScope.msgAlertaLinha1 = msgAlertaLinha1;
             $rootScope.msgAlertaLinha2 = msgAlertaLinha2;
             $rootScope.tituloMsgAlerta = titulo;
             $rootScope.tipoMsgAlerta = tipo;
             $('#modalAlerta').modal('show');
        }, // ModalAlerta

        toggleOverlay: function () {
            $rootScope.showOverlay = !$rootScope.showOverlay;
        },

        cleanCPF: function (cpf) {
            return cpf.replace(".", "").replace(".","").replace("-","");
        },

        getDDD: function (fone) {
            return fone.substr(1, 2);
        },

        getFone: function (fone) {
            return fone.substr(4, fone.length).replace("-","").replace(" ","");
        },

        xml2Json: xmlToJson,
        LimpaText: LimpaTagText
    }
}
    

    
    