App
.factory('Auth', autenticacao);

function autenticacao($firebase) {
    return {
         
        jsonUsuario: function (nome, email, primeiroNome){
            return {
                nome: nome,
                email: email,
                primeiroNome: primeiroNome
            };
        },
        
        
        mensagemErroLogin: function (error){
            var msg = '';
            switch (error.code) {
                case "auth/invalid-email":
                    msg = 'O e-mail informado é inválido.';
                    break;
                case "auth/user-disabled":
                    msg = 'O usuário não está ativo.';
                    break;
                case "auth/user-not-found":
                    msg = 'Não existe usuário para o e-mail informado.';
                    break;
                case "auth/wrong-password":
                    msg = 'A senha está incorreta.';
                    break;
                case "auth/email-already-in-use":
                    msg = 'Já existe um usuário com este e-mail.';
                    break;
                case "auth/weak-password":
                    msg = 'A senha informada é muito fraca.';
                    break;
                default:
                    msg = error.message;
            }
            return '<strong>Ops!</strong> Falha na operação: ' + msg;
        },
        
        login: function(user, password){
            firebase.auth().signInWithEmailAndPassword(user, password)
                .catch(function(error) {
                    console.log(error.code);
                    console.log(error.message);
                });
        },
        
        toggleLogin: function (user) {
            if (firebase.auth().currentUser) {
                firebase.auth().signOut();
            } 
            else {
                firebase.auth().signInAnonymously().catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if (errorCode === 'auth/operation-not-allowed') {
                        alert('You must enable Anonymous auth in the Firebase Console.');
                    } 
                    else {
                        console.error(error);
                    }
                });
            }
        }
    }
}
