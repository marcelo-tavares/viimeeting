App
.factory('BD', ['$rootScope', '$http', '$firebase', '$firebaseArray', 'Dados', bdFactory]);

function bdFactory($rootScope, $http, $firebase, $firebaseArray, Dados) {
    var itens = [];

    return {
        get: function (filho) {
            try {
                ref = Dados.getRefDados();
                itens = Dados.toArray(ref, filho);
            }
            catch (err) {
                alert(err.message);
                itens = [];
            }
            finally {
                return itens;
            }
        },

        getFiltro: function (filho, campo, valor) {
            try {
                ref = Dados.getRefDados();
                itens = Dados.toArrayFiltro(ref, filho, campo, valor);
            }
            catch (err) {
                alert(err.message);
                itens = [];
            }
            finally {
                return itens;
            }
        },

        getFiltroOp: function (filho, campo, operacao, valor) {
            try {
                ref = Dados.getRefDados();
                itens = Dados.toArrayFiltroOp(ref, filho, campo, operacao, valor);
            }
            catch (err) {
                alert(err.message);
                itens = [];
            }
            finally {
                return itens;
            }
        },

        getObj: function (filho) {
            var obj;
            var urlFilho = Dados.getUrlDados() + filho + '.json';
            $http({
                method: "GET",
                url: urlFilho
            }).then(function sucesso(resposta) {
                obj = resposta.data;
            }, function erro(resposta) {
                obj = {};
                console.log(resposta.statusText);
            });

            return obj;
        },

        atualizar: function (filho, id, item) {
            var itemRef = Dados.getRefDadosFilho(filho)
            try {
                itemRef.child(id).set(item);
            }
            catch (err) {
                alert(err);
            }
            finally {
                return;
            }
        },

        inserir: function (filho, novoItem) {
            var retorno = [];
            urlFilho = Dados.getUrlDadosFilho(filho);
            $http({
                method: "POST",
                url: urlFilho,
                data: novoItem
            }).then(
                function (resposta){
                    return resposta;
                },
                function erro(resposta){
                    return resposta;
                }
            )
        },
        
        writeNew: function (node, novoItem){
            var newNodeKey = firebase.database().ref().child(node).push().key;
            var update = {};
            update[node + '/' + newNodeKey] = novoItem;
            
            return firebase.database().ref().update(update);
        },

        remover: function (filho, id) {
            url = Dados.getUrlDadosFilhoId(filho, id);
            try {
                $http.delete(url);
            }
            catch (err) {
                alert(err);
            }
            finally {
                return;
            }
        },

        gravaTransacaoPS: function (transacao) {
            console.log($rootScope.participante)
            console.log(transacao);
        }

    }
}
