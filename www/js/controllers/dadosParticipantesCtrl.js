App.controller('dadosParticipantesCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', 'Inscricao', dadosParticipantesController]);

function dadosParticipantesController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc, Inscricao) {

    function getParticipantes() {
        var participantesRef = Dados.getRefDadosFilho('participantes', $rootScope.sandbox);
        return participantesRef.once('value').then(populaParticipantes);
    }

    function montaItemArray(i, item) {
        return {
            id: item.id,
            nome: item.nome,
            categoria: item.categoria,
            fbid: i
        }
    }

    function montaArray(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            obj = { id: item.id,
                    nome: item.nome,
                    email: item.email,
                    endereco: item.endereco,
                    telefone: item.telefone,
                    categoria: item.categoria,
                    fbid: i
            }
            arr.push(obj);
        });
        return arr;
    }

    function populaParticipantes(snapshot){
        var arrParticipantes = montaArray(snapshot.val());
        $scope.participantes = arrParticipantes;
        $scope.$apply();
    }

    $scope.getCategoria = function (id) {
        if (id > 90)
            id -= 90;
        return Misc.getCategoria(id);
    }

    $scope.getComplemento = function(complemento) {
        return Misc.getComplemento(complemento)
    }

    $scope.getTipoInscricao = function(grupo) {
        return Misc.getTipoInscricao(grupo);
    }

    $scope.toggleCampos = function () {
        $('#modalCampos').modal('show');
    }

    $scope.atualizaListaParticipantes = function () {
        getParticipantes();             
    }

    $scope.limparFiltros = function() {
        $scope.filtro = {
            numero:"",
            nome: "",
            situacao: "",
            categoria: "",
            grupo: "",
            ocultarGrupos: "",
            ocultarCortesias: ""
        };
    }

    $scope.inicializarCampos = function () {
        $scope.campo = {
            id: true,
            nome: true,
            email: true,
            categoria: true,
            telefone: true,
            logradouro: true,
            numero: true,
            complemento: true,
            bairro: true,
            cidade: true,
            estado: true,
                cep: true
            }
        }

        limparOrdenacao = function() {
            $scope.ordenacao = "id"
        }

        $scope.sortBy = function(propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.propertyName = 'id';
        $scope.reverse = false;

        //Buscar e apresentar as inscrições
        getParticipantes();

        $scope.limparFiltros();
        $scope.inicializarCampos();
}



