App
.controller('signUpCtrl', ['$scope', '$rootScope', '$state', '$firebase', 'Dados', 'Auth', 'Misc', signUpController]);

function signUpController($scope, $rootScope,$state, $firebase, Dados, Auth, Misc){

    $scope.abrirSignIn = function() {
        $state.go('app.signin')
    }

    $scope.validaNome = function() {
        $scope.validaFormatoNome();
        if ($scope.nomeValido == 0) {
            $scope.msgNome = "O campo Nome Completo deve ser informado.";
            $scope.classeNome = "has-error";
        }
        else
            if ($scope.nomeValido == 2) {
                $scope.msgNome = "O formato do Nome é inválido (informe pelo menos nome e um sobrenome).";
                $scope.classeNome = "has-error";
            } else {
                $scope.msgNome = "";
                $scope.classeNome = "";
            }
    }

    $scope.validaFormatoNome = function() {
        var nomePattern =  /^[a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÒÖÚÇÑ1234567890 ]+$/;
        if ($scope.inputNome.length == 0)
            $scope.nomeValido = 0;
        else
            if (Misc.validaExpReg($scope.inputNome, nomePattern) && ($scope.inputNome.indexOf(" ") >= 0)){
                $scope.nomeValido = 1;
            }
            else {
                $scope.nomeValido = 2;
            }
        return;
    }


    $scope.validaEmail = function() {
        if ($scope.emailValido == 0) {
            $scope.msgEmail = "O campo E-mail deve ser informado.";
            $scope.classeEmail = "has-error";
        }
        else
            if ($scope.emailValido == 2) {
                $scope.msgEmail = "O formato do E-mail é inválido.";
                $scope.classeEmail = "has-error";
            } else {
                $scope.msgEmail = "";
                $scope.classeEmail = "";
            }
    }

    $scope.validaFormatoEmail = function() {
        var emailPattern =  /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;
        if ($scope.inputUser.length == 0)
            $scope.emailValido = 0;
        else
            if (Misc.validaExpReg($scope.inputUser, emailPattern)){
                $scope.emailValido = 1;
            }
            else {
                $scope.emailValido = 2;
            }
        return;
    }

    $scope.validaFormatoSenha = function() {
        if ($scope.inputPassword.length == 0)
            $scope.senhaValida = 0;
        else
            if ($scope.inputPassword.length >= 8){
                $scope.senhaValida = 1;
            }
            else {
                $scope.senhaValida = 2;
            }
        return;
    }

    $scope.validaSenha = function() {
        if ($scope.inputPassword.length == 0){
            $scope.msgSenha = "O campo Senha deve ser informado.";
            $scope.classeSenha = "has-error";
        }
        else {
            if ($scope.inputPassword.length < 8){
                $scope.msgSenha = "O campo Senha deve ter pelo menos 8 caracteres.";
                $scope.classeSenha = "has-error";
            }
            else {
                $scope.msgSenha = "";
                $scope.classeSenha = "";
            }
        }

    }

    $scope.validaFormatoSenhaRep = function() {
        if ($scope.inputPasswordRep.length == 0)
            $scope.senhaRepValida = 0;
        else
            if ($scope.inputPassword === $scope.inputPasswordRep){
                $scope.senhaRepValida = 1;
            }
            else {
                $scope.senhaRepValida = 2;
            }
        return;
    }

    $scope.validaSenhaRep = function() {
        if ($scope.inputPassword != $scope.inputPasswordRep){
            $scope.msgSenhaRep = "O campo Repetir senha não coincide com a senha informada.";
            $scope.classeSenhaRep = "has-error";
        }
        else {
            $scope.msgSenhaRep = "";
            $scope.classeSenhaRep = "";
        }
    }

    $scope.fecharMsgEmail = function () {
        $scope.msgEmail = "";
        $scope.classeEmail = "";
    }

    $scope.fecharMsgSenha = function () {
        $scope.msgSenha = "";
        $scope.classeSenha = "";
    }

    $scope.fecharMsgSenhaRep = function () {
        $scope.msgSenhaRep = "";
        $scope.classeSenhaRep = "";
    }

    fechaMsg = function () {
        $scope.flagErro = false;
        $scope.flagSucesso = false;
    }



    $scope.criarUsuario = function (){
        fechaMsg();
        firebase.auth().createUserWithEmailAndPassword($scope.inputUser, $scope.inputPassword)
            .then(
                function () {
                    firebase.auth().currentUser.updateProfile({displayName: $scope.inputNome})
                        .then(
                            function() {
                                $scope.flagSucesso = true;
                                $scope.msgSucesso = '<strong>Ok!</strong> usuário criado com sucesso';
                                firebase.auth().currentUser.displayName = $scope.inputNome;
                                $scope.usuario.nome = $scope.inputNome;
                                $scope.usuario.email = $scope.inputUser;
                                $scope.$apply();

                                $rootScope.adminUser = false;
                                $rootScope.$apply();

                                $state.go('app.myMeeting.perfil');
                            },
                            function(error) {
                                $scope.flagErro = true;
                                $scope.msgErro = Auth.mensagemErroLogin(error);
                                $scope.usuario = Auth.jsonUsuario('','','');
                                $scope.$apply();

                                $rootScope.adminUser = false;
                                $rootScope.$apply();

                            }
                    );
                }
            )
            .catch(
                function(error) {
                    $scope.flagErro = true;
                    $scope.msgErro = Auth.mensagemErroLogin(error);
                    $scope.usuario = Auth.jsonUsuario('','','');
                    $scope.$apply();
                }
            );
    }

    // ------------------------
    $scope.emailValido = 0;
    $scope.senhaValida = 0;
    $scope.inputPassword = '';
    $scope.inputPasswordRep = '';
    $scope.inputUser = '';
    $scope.inputNome = '';

    $scope.usuario = Auth.jsonUsuario('','','');

    if (firebase.auth().currentUser)
        $state.go('app.myMeeting.perfil');
}
