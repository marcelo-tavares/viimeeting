App.controller('pagamentoDadosBoletoCtrl',
              ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'BD', 'Auth', 'Misc', pagamentoDadosBoletoController]);

function pagamentoDadosBoletoController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, BD, Auth, Misc) {

    $scope.abreMeiosPagamento = function() {
        $state.go('app.myMeeting.inscricao.pagamento.meios')
    }

    $scope.abreConclusao = function() {
        //$state.go('app.myMeeting.inscricao.pagamento.meios')
    }

    montaParametros = function () {

        var emailSender = $rootScope.participante.email;
        if ($rootScope.sandbox){
            emailSender = 'c55408407219930469592@sandbox.pagseguro.com.br'
        }

        var jsonPagto = {
            paymentMode:'default',
            paymentMethod:'boleto',
            receiverEmail: 'scepd2017@gmail.com',
            currency: 'BRL',
            extraAmount: '0.00',
            itemId1: '01',
            itemDescription1: 'Meeting SCPED - Inscrição Nº' + $rootScope.participante.id,
            itemAmount1: Misc.buscaValorInscricao($rootScope.participante.categoria),
            itemQuantity1: '1',
            reference: $rootScope.idInscricaoUsuario,
            senderName: $rootScope.participante.nome,
            senderCPF: Misc.cleanCPF($rootScope.participante.cpf),
            senderAreaCode: Misc.getDDD($rootScope.participante.telefone),
            senderPhone: Misc.getFone($rootScope.participante.telefone),
            senderEmail: emailSender,
            senderHash: PagSeguroDirectPayment.getSenderHash(),
            shippingAddressStreet: $rootScope.participante.endereco.logradouroCompleto,
            shippingAddressNumber: $rootScope.participante.endereco.numero,
            shippingAddressComplement: $rootScope.participante.endereco.complemento,
            shippingAddressDistrict: $rootScope.participante.endereco.bairro,
            shippingAddressPostalCode: $rootScope.participante.endereco.cep,
            shippingAddressCity: $rootScope.participante.endereco.cidade,
            shippingAddressState: $rootScope.participante.endereco.estado,
            shippingAddressCountry: 'Brasil',
            shippingType: '3',
            shippingCost: '0.00'
        }
        return jsonPagto;
    } /* montaParametros */

    atualizarTransacaoOk = function (erro) {
        if (erro) {
            Misc.modalAlerta('Erro ao gravar transação', 'Tente efetuar novamente pelo botão na sessão Dados de Pagamento de sua Inscrição!', 'Ops!!!', 'danger');
        }
        else
        {
            Misc.modalAlerta('Transação gravada com sucesso!', 'Imprima seu boleto pelo botão na sessão Dados de Pagamento de sua Inscrição!', 'Ok!!!', 'success');
            $state.go('app.myMeeting.inscricao.minhaInscricao');
        }
        $("#overlay").hide();
    }

    atualizarTransacao = function (transacao) {
        var ref = Dados.getRefDadosFilho('participantes/' + $rootScope.idInscricaoUsuario, $rootScope.sandbox)
        ref.update(transacao, atualizarTransacaoOk);
    }

    doPagtoBoleto = function () {
        $("#overlay").show();
        var url = 'pagamento/pagamento.php'
        if ($rootScope.sandbox){
            url = 'pagamento/pagamento_sandbox.php'
        }
        var params = montaParametros();
        var ajaxConfig = {
            type:'POST',
            url: url,
            data: params,
            cache: false,
            success: doPagtoSucesso,
            error: doPagtoFalha
        }
        $.ajax(ajaxConfig);
    } /* doPagtoBoleto */

    function doPagtoSucesso(response) {
        json = Misc.xml2Json($.parseXML(response));

        //TODO: Testar se o json está trazendo erros.
        if (json.transaction){
            // Retirar #Text do json
            json = Misc.LimpaText(json)

            // Gravar transação no BD
            atualizarTransacao(json);
        } else {
            // abrir modal de mensagem de Erro
            msgErro = json.errors.error.code["#text"] + " - " + json.errors.error.message["#text"];
            Misc.modalAlerta('Erro ao gravar transação', msgErro, 'Ops!!!', 'danger');

            $("#overlay").hide();
        }
    }

    function doPagtoFalha(jqxhr) {
        // obtendo lista de erros
        //var response = $.parseJSON(jqxhr.responseText);

        // Exibindo lista de erros
        //showPaymentErrors(response.errors);

            Misc.modalAlerta('Erro ao processar a transação', '', 'Ops!!!', 'danger');
            $("#overlay").hide();
    }


    doPagtoBoleto();


} /* pagamentoDadosBoletoController */
