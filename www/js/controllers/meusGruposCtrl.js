App.controller('meusGruposCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', '$sce', 'Dados', 'Misc', meusGruposController]);

function meusGruposController ($scope, $rootScope, $state, $firebase, $firebaseArray, $sce, Dados, Misc) {


     function montaArrayGrupos(data) {
        var arr = Array();
        var obj = {};
        $.each(data, function(i, item) {
            if ($rootScope.idInscricaoUsuario == item.administrador.fbid) {
                obj = { nome: item.nome,
                        administrador: item.administrador,
                        botao: $sce.trustAsHtml(item.botaoPgto),
                        fbid: i
                }
                arr.push(obj);
                $scope.$apply();
            }
        });
        return arr;
    }

    function populaGrupos(snapshot){
        var arrGrupos = montaArrayGrupos(snapshot.val());
        $rootScope.gruposParticipante = arrGrupos;
        if (arrGrupos.length > 0)
            $scope.adminGrupo = true;
        else
            $scope.adminGrupo = false;
        $scope.resultadoBuscaOk = true;

        $rootScope.$apply();
    }

    function getGruposAdministrador() {
        $scope.resultadoBuscaOk = false;
        var gruposRef = Dados.getRefDadosFilho('grupos', $rootScope.sandbox);
        return gruposRef.once('value').then(populaGrupos);
    }

    function getParticipantes() {
        var participantesRef = Dados.getRefDadosFilho('participantes', $rootScope.sandbox);
        return participantesRef.once('value').then(populaParticipantes);
    }

    function montaArrayParticipantes(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            if (item.grupo.nome != undefined) {
                obj = { id: item.id,
                        nome: item.nome,
                        categoria: item.categoria,
                        grupo: item.grupo,
                        fbid: i
                }
                arr.push(obj);
            }
        });
        return arr;
    }

    function populaParticipantes(snapshot){
        var arrParticipantes = montaArrayParticipantes(snapshot.val());
        $scope.participantes = arrParticipantes;
        $scope.$apply();
        getGruposAdministrador();
    }

    $scope.participantesGrupo = function (grupo) {
        var strParticipantes = "";
        var p = $scope.participantes;
        var pg = 0;
        for (i=0; i<p.length; i++)
            if (p[i].grupo.fbid == grupo) {
                strParticipantes +=   "<li>" + "[" + formataIdInscricao(p[i].id) + "] " + p[i].nome + " (" + getCategoria(p[i].categoria) + ") </li>";
                pg+=1;
        }
        if (pg == 0)
            strParticipantes = "<li>Nenhum participante identificado nesse grupo.</li>";


        return $sce.trustAsHtml(strParticipantes);
    }

  formataIdInscricao = function(id){
        return Misc.formataIdInscricao(id);
    }

    getCategoria = function (id) {
        return Misc.getCategoria(id);
    }

    // -----
    getParticipantes();
}
