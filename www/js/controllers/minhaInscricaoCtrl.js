﻿App.controller('minhaInscricaoCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', minhaInscricaoController]);

function minhaInscricaoController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc) {
    $rootScope.pagamento = false;

    $scope.efetuarInscricaoIndividual = function() {
        //$state.go('app.myMeeting.individual');
		Misc.modalAlerta('Inscrições encerradas para esse evento', '', 'Ops!!!', 'danger');
    }

    $scope.buscarInscricaoUsuarioCorrente = function() {
        var retorno = null;

        if (firebase.auth().currentUser) {
            var uid = firebase.auth().currentUser.uid;
            var ref = firebase.database().ref(Dados.getRoot($rootScope.sandbox) + '/participantes');
            $scope.resultadoBuscaOk = false;
            ref.orderByChild('uid').equalTo(uid).once('value')
                .then(
                    function(snapshot) {
                        if (!snapshot.hasChildren()) {
                            $scope.resultadoBuscaOk = true;
                            $scope.idInscricaoUsuario = null;
                            $scope.dadosInscricaoUsuario = null;
                            $scope.$apply();
                            $("#overlay").hide();
                        }
                        else {
                            snapshot.forEach(
                                function(childSnapshot) {
                                    key = childSnapshot.key;
                                    participante = childSnapshot.val();

                                    $scope.resultadoBuscaOk = true;
                                    $rootScope.idInscricaoUsuario = key;
                                    $rootScope.participante = participante;
                                    $rootScope.$apply();

                                    $scope.botaoPagamento = mostrarBotaoPagamento(participante);
                                    $scope.detalhesTransacao = mostrarDetalhesTransacao(participante);
                                    $scope.certificado = mostrarCertificado(participante);
                                    $scope.$apply();
                                    $("#overlay").hide();
                                }
                            );
                        }
                    }
                )
                .catch(
                    function (error) {
                        $scope.resultadoBuscaOk = true;
                        $scope.idInscricaoUsuario = null;
                        $scope.dadosInscricaoUsuario = null;
                        $scope.$apply();
                        $("#overlay").hide();
                    }
                );
        }
    }

    $scope.formataIdInscricao = function(id){
        return Misc.formataIdInscricao(id);
    }

    $scope.getCategoria = function (id) {
        return Misc.getCategoria(id);
    }

    $scope.getComplemento = function(complemento) {
        return Misc.getComplemento(complemento)
    }

    $scope.getTipoInscricao = function(grupo) {
        return Misc.getTipoInscricao(grupo);
    }

    $scope.getMetodoTransacao = function(metodo, parcelas) {
        return Misc.getMetodoTransacao(metodo, parcelas);
    }

    $scope.getStatusTransacao = function(status) {
        return Misc.getStatusTransacao(status);
    }

    $scope.getValorTransacao = function(valor, metodo) {
        return Misc.getValorTransacao(valor, metodo);
    }

    $scope.getDataPagamento = function(data) {
        return Misc.getDataFormatada(data);
    }

    $scope.doPgto = function() {
        $("#overlay").show();
        if ($rootScope.sandbox)
            $state.go('app.myMeeting.inscricao.pagamento.sessionSandbox');
        else
            $state.go('app.myMeeting.inscricao.pagamento.session');
    }

    $scope.boletoNaoPago = function() {
        var retorno = false;
        participante = $rootScope.participante;
        if (participante != undefined) {
            if (participante.transaction != undefined){
                if ((participante.transaction.status == "1") && (participante.transaction.paymentMethod.type == "2"))
                    retorno = true;
            }
        }

        return retorno;
    }

    mostrarBotaoPagamento = function (participante){
        var retorno = false;
        if (participante != undefined) {
            if ((participante.categoria != 1) && (participante.categoria < 90)) {
                if (participante.transaction != undefined) {
                    if (participante.transaction != ""){
                        if ((participante.transaction.status == "6") || (participante.transaction.status == "7")) {
                            retorno = true;
                        }
                    }
                    else {
                        retorno = true;
                    }
                }
                else {
                    retorno = true;
                }
            }
        }

        return retorno;
     }

    mostrarDetalhesTransacao = function (participante){
        var retorno = false;
        if (participante != undefined) {
            if (participante.transaction != undefined) {
                if (participante.transaction != ""){
                    if ((participante.transaction.status != "6") && (participante.transaction.status != "7")) {
                        retorno = true;
                    }
                }
            }
        }

        return retorno;
     }

    mostrarCertificado = function (participante){
        var retorno = false;
        if (participante != undefined) {
            if (participante.certificado != undefined) {
                if (participante.certificado != ""){
                    retorno = true;
                }
            }
        }

        //true para todos
        retorno = true;

        return retorno;
     }

    // -----
    $scope.buscarInscricaoUsuarioCorrente();

}
