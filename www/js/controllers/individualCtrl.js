App.controller('individualCtrl', ['$scope', '$rootScope', '$state', '$sce', '$http', 'Inscricao', 'Dados', 'BD', 'Auth', 'Misc', individualController]);

function individualController($scope, $rootScope, $state, $sce, $http, Inscricao, Dados, BD, Auth, Misc){

    inscricaoValida = function (){
        var valida = true;
        var strMsg = '';
        var participante = $scope.participante;
        $scope.mensagensErro = "";

        $scope.classeNome = "";
        $scope.msgNome = "";
        if (participante.nome.length == 0) {
            valida = false;
            $scope.msgNome = "Nome não informado";
            $scope.classeNome = "has-error";
        }

        $scope.classeCPF = "";
        $scope.msgCPF = "";
        if (participante.cpf.length == 0) {
            valida = false;
            $scope.classeCPF = "has-error";
            $scope.msgCPF = "CPF não informado";
        }

        if ($scope.cpfValido == 2)  {
            valida = false;
            $scope.classeCPF = "has-error";
            $scope.msgCPF = "CPF Inválido";
        }

        if ($scope.cpfValido == 3)  {
            valida = false;
            $scope.classeCPF = "has-error";
            $scope.msgCPF = "Já existe uma inscrição com esse CPF";
        }

        $scope.classeEmail = "";
        $scope.msgEmail = "";
        if (participante.email.length == 0) {
            valida = false;
            $scope.classeEmail = "has-error";
            $scope.msgEmail = "E-mail não informado";

        }

        $scope.classeTelefone = "";
        $scope.msgTelefone = "";
        if (participante.telefone.length == 0)  {
            valida = false;
            $scope.classeTelefone = "has-error";
            $scope.msgTelefone = "Telefone não informado";
        }

        if ($scope.telefoneValido > 1)  {
            valida = false;
            $scope.classeTelefone = "has-error";
            $scope.msgTelefone = "Telefone Inválido";
        }

        $scope.classeCategoria = "";
        $scope.msgCategoria = "";
        if (participante.categoria == 0)  {
            valida = false;
            $scope.classeCategoria = "has-error";
            $scope.msgCategoria = "Categoria não informada";
        }

        $scope.classeCEP = "";
        $scope.msgCEP = ""
        if ($scope.cepValido > 1)  {
            valida = false;
            $scope.msgCEP = "CEP Inválido";
            $scope.classeCEP = "has-error";
        }

        if (participante.endereco.cep.length == 0)  {
            valida = false;
            $scope.msgCEP = "CEP não informado";
            $scope.classeCEP = "has-error";
        }

        $scope.classeLogradouro = "";
        $scope.msgLogradouro = "";
        if (participante.endereco.logradouroCompleto.trim().length == 0)  {
            valida = false;
            $scope.msgLogradouro = "Logradouro não informado";
            $scope.classeLogradouro = "has-error";
        }

        $scope.classeNumero = "";
        $scope.msgNumero = "";
        if (participante.endereco.numero.length == 0)  {
            valida = false;
            $scope.msgNumero = "Número (endereço) não informado";
            $scope.classeNumero = "has-error";
        }

        $scope.classeBairro = "";
        $scope.msgBairro = "";
        if (participante.endereco.bairro.length == 0)  {
            valida = false;
            $scope.msgBairro = "Bairro não informado";
            $scope.classeBairro = "has-error";
        }

        $scope.classeCidade = "";
        $scope.msgCidade = "";
        if (participante.endereco.cidade.length == 0)  {
            valida = false;
            $scope.msgCidade = "Cidade não informada";
            $scope.classeCidade = "has-error";
        }

        $scope.classeEstado = "";
        $scope.msgEstado = "";
        if (participante.endereco.estado.length == 0)  {
            valida = false;
            $scope.msgEstado = "Estado não Informado";
            $scope.classeEstado = "has-error";
        }

        if (!valida) {
            //$scope.resizeFrame(1700, '1700px');
            alert('Erros no formúlário de Inscrição. Por favor corrija e tente novamente.');
        }
        return valida;
    }

    $scope.resetMsg = function(campo){
        switch (campo) {
            case 'cpf':
                $scope.msgCPF = "";
                $scope.classeCPF = "";
                break;
            case 'email':
                $scope.msgEmail = "";
                $scope.classeEmail= "";
                break;
            case 'fone':
                $scope.msgTelefone = "";
                $scope.classeTelefone= "";
                break;
            case 'cep':
                $scope.msgCEP  = "";
                $scope.classeCEP = "";
                break;
            case 'logradouro':
                $scope.msgLogradouro = "";
                $scope.classeLogradouro= "";
                break;
            case 'numero':
                $scope.msgNumero = "";
                $scope.classeNumero= "";
                break;
            case 'bairro':
                $scope.msgBairro = "";
                $scope.classeBairro = "";
                break;
            case 'cidade':
                $scope.msgCidade = "";
                $scope.classeCidade= "";
                break;
            case 'estado':
                $scope.msgEstado = "";
                $scope.classeEstado= "";
                break;
        }
    }

    $scope.mostrarCRO = function() {
        mostrar = false;
        if (($scope.participante.categoria == 1) ||
            ($scope.participante.categoria == 2) ||
            ($scope.participante.categoria == 3))
            mostrar = true;
        return mostrar
    }

    $scope.mostrarInstituicao = function() {
        mostrar = false;
        if (($scope.participante.categoria == 4))
            mostrar = true;
        return mostrar
    }

    $scope.getUltimaInscricao = function () {
         obj = BD.getObj('config/ultimaInscricao');
         a=1;
     }

    $scope.montaProduto = function(categoria){
         switch (categoria){
             case 2:
                 $scope.inscricao.descricao = 'Adesão ao VII Meeting da SCEPD para Sócios da SBRO';
                 $scope.inscricao.valor = '400';
                 break;
             case 3:
                 $scope.inscricao.descricao = 'Adesão ao VII Meeting da SCEPD para CDs';
                 $scope.inscricao.valor = '600';
                 break;
             case 5:
                 $scope.inscricao.descricao = 'Adesão ao VII Meeting da SCEPD para TPDs';
                 $scope.inscricao.valor = '200';
                 break;
             case 4:
                 $scope.inscricao.descricao = 'Adesão ao VII Meeting da SCEPD para Acadêmicos';
                 $scope.inscricao.valor = '300';
                 break;
         }
     }

    criaMensagemHtml = function(participante) {
        mensagem = '<p>Número da Inscrição<br><strong>' + participante.id + '</strong></p>' +
                    '<p>Tipo de Inscrição<br><strong>' + Misc.getTipoInscricao(participante.grupo) + '</strong></p>' +
                    '<p>Data de Inscrição<br><strong>' + participante.dataInscricao + '</strong></p>'+
                    '<p>Nome<br><strong>' + participante.nome + '</strong></p>'+
                    '<p>CPF<br><strong>' + participante.cpf + '</strong></p>'+
                    '<p>E-mail<br><strong>' + participante.email + '</strong></p>'+
                    '<p>Telefone<br><strong>' + participante.telefone + '</strong></p>'+
                    '<p>Categoria<br><strong>' + Misc.getCategoria(participante.categoria) + '</strong></p>'+
                    '<p>Endereço<br><strong>'+
                    participante.endereco.logradouroCompleto + ', ' +
                    participante.endereco.numero +
                    Misc.getComplemento(participante.endereco.complemento) + '<br>' +
                    participante.endereco.bairro + ' - ' +
                    participante.endereco.cidade + ' - ' +
                    participante.endereco.estado + '<br>' +
                    participante.endereco.cep + '</strong></p>'
        return mensagem;
    }

    enviaEmail = function(para, assunto, mensagem){
        dadosEmail = {
            nomeRemetente: 'Secretaria do VII Meeting da SCEPD',
            emailRemetente: 'secretaria@scepd.meeting.net.br',
            emailDestinatario: para,
            emailCc: '',
            emailCco: '',
            assunto: assunto,
            mensagem: mensagem
        }

        Misc.enviaEmail(dadosEmail);
    }

    verificaInscricaoComCPF = function() {
        var ref = firebase.database().ref(Dados.getRoot($rootScope.sandbox) + '/participantes');
        $scope.resultadoBuscaOk = false;
        ref.orderByChild('cpf').equalTo($scope.participante.cpf).once('value')
            .then(
                function(snapshot) {
                    if (!snapshot.hasChildren()) {
                        $scope.resultadoBuscaOk = true;
                        $scope.existeInscricaoComCPF = false;
                        $scope.classeCPF = '';
                        $scope.msgCPF = '';
                        $scope.$apply();
                    }
                    else {
                        $scope.resultadoBuscaOk = true;
                        $scope.existeInscricaoComCPF = true;
                        $scope.classeCPF = 'has-error';
                        $scope.msgCPF = "Já existe uma inscrição para esse CPF";
                        $scope.cpfValido = 3;
                        $scope.$apply();
                    }
                }
            )
            .catch(
                function (error) {
                    $scope.resultadoBuscaOk = true;
                    $scope.existeInscricaoComCPF = false;
                    $scope.$apply();
                    console.log(error.message);
                }
            );

    }

    cpfOk = function(verificaCpfExistente){
        $scope.classeCPF = '';
        $scope.msgCPF = '';
        if ($scope.participante.cpf.length == 0)
            $scope.cpfValido = 0;
        else {
            if (Inscricao.validaCPF($scope.participante.cpf)){
                $scope.cpfValido = 1;
                if (verificaCpfExistente)
                    verificaInscricaoComCPF();
            }
            else
                $scope.cpfValido = 2;
        }
        return;
    }

    $scope.enviarInscricao = function() {
        if (navigator.onLine) {
            if (inscricaoValida()){
                $("#overlay").show();
                var ultimaInscricaoRef = Dados.getRefDadosFilho('config/ultimaInscricao', $rootScope.sandbox);

                ultimaInscricaoRef.transaction(
                    // Numa transação incrementa o valor da ultima inscrição realizada.
                    // Caso duas ou mais pessoas tentem se inscrever simultaneamente
                    // A transação trata a situação dando um número diferente para cada inscrição
                    function (ultima) {
                        return ultima + 1;
                    },
                    // Ao completar a operação verifica se houve erro ou não.
                    // Caso o commit tenha sido feito, grava a nova inscrição
                    function (error, committed, snapshot) {
                        if (error){
                            Misc.modalAlerta('Erro ao reservar a nova inscrição.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                        }
                        else
                            if (!committed)
                                Misc.modalAlerta('Erro ao reservar a nova inscrição (1).', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                            else {
                                // Gravar nova Inscricao
                                data = new Date();

                                var id = snapshot.val();

                                $scope.participante.id = id;
                                $scope.participante.uid = firebase.auth().currentUser.uid;
                                $scope.participante.pago = 0;
                                $scope.participante.dataInscricao = data.toLocaleDateString();
                                $scope.participante.dataPagamento = '';
                                $scope.participante.grupo = '';
                                $scope.participante.emailEnviado = 0;

                                var participanteRef = Dados.getRefDados();
                                var newNodeKey = participanteRef.child('participantes').push().key;
                                var dados = {};
                                dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + newNodeKey] = $scope.participante;
                                firebase.database().ref().update(dados)
                                    .then(
                                        function(){

                                            // enviar mensagem
                                            var assunto = "Sua Inscrição do VII Meeting da SCEPD / XVI Encontro Regional da SBRO";
                                            var mensagem = criaMensagemHtml($scope.participante);
                                            enviaEmail($scope.participante.email, assunto, mensagem);

                                            $scope.hashInscricao = newNodeKey;
                                            $scope.pagamento = true;
                                            $scope.$apply();

                                            $rootScope.idInscricaoUsuario = newNodeKey;
                                            $rootScope.participante = $scope.participante;
                                            $("#overlay").hide();
                                        }
                                    ).catch(
                                        function(error) {
                                             Misc.modalAlerta('Erro ao gravar inscrição.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                                             $("#overlay").hide();
                                        }
                                    )
                            }
                            window.parent.document.body.scrollTop = 0;
                    },
                    false // não ver estados intermediários
                );
            }
        }
        else {
            Misc.modalAlerta('Sem conexão com a internet!', 'Corrija o problema e envie sua inscrição.', 'Ops!!!', 'danger');
        }

    }

    resetEndereco = function(){
        $scope.participante.endereco.logradouro = "";
        $scope.participante.endereco.logradouroCompleto = "";
        $scope.participante.endereco.bairro = "";
        $scope.participante.endereco.cidade = "";
        $scope.participante.endereco.estado = "";
        $scope.participante.endereco.tipoLogradouro = "";
        $scope.participante.endereco.cep = "";
        $scope.participante.endereco.numero = "";
        $scope.participante.endereco.complemento = "";
    }

    $scope.buscaPorCEP = function (){
        if ($scope.participante.endereco.cep.length == 0){
            $scope.cepValido = 0;
            return;
        }

        if ($scope.participante.endereco.cep.length < 8){
            $scope.cepValido = 2;
            return;
        }

        $scope.buscandoCEP = true;
        $("#overlay").show();
        /*
        $.ajax({
            url: 'https://correiosapi.apphb.com/cep/' + $scope.participante.endereco.cep.replace(/\D/g, ''),
            dataType: 'jsonp',
            crossDomain: true,
            contentType: "application/json",
            statusCode: {
                200: function(data) { // Ok
                        $scope.statusEndereco = 200;
                        $scope.participante.endereco = Inscricao.jsonEndereco(data.cep, data.tipoDeLogradouro,
                                                                              data.logradouro, '', '', data.bairro,
                                                                              data.cidade, data.estado,
                                                                              data.tipoDeLogradouro + ' ' + data.logradouro);
                        $scope.buscandoCEP = false;
                        $scope.classeCEP = "";
                        $scope.msgCEP = "";
                        $scope.cepValido = 1;
                        $scope.$apply();
                        $("#overlay").hide();
                    },
                400: function(msg)  {  // Bad Request
                        $scope.statusEndereco = 400;
                        $scope.buscandoCEP = false;
                        $scope.msgCEP = msg;
                        $scope.classeCEP = "has-error";
                        $scope.cepValido = 2;
                        resetEndereco();
                        $scope.$apply();
                        $("#overlay").hide();
                    },
                404: function(msg) { // Not Found
                        $scope.statusEndereco = 404;
                        $scope.buscandoCEP = false;
                        $scope.msgCEP = "CEP não encontrado!!";
                        $scope.classeCEP = "has-error";
                        $scope.cepValido = 2;
                        resetEndereco();
                        $scope.$apply();
                        $("#overlay").hide();
                    },
                0: function() {
                        $("#overlay").hide();
                   }
            }
        });
        */
        $.ajax({
                url: 'https://correiosapi.apphb.com/cep/' + $scope.participante.endereco.cep.replace(/\D/g, ''),
                dataType: 'jsonp',
                crossDomain: true,
                timeout: 5000,
                contentType: "application/json"}
        )
        .done(
            function (data) {
                $scope.statusEndereco = 200;
                $scope.participante.endereco = Inscricao.jsonEndereco(data.cep, data.tipoDeLogradouro,
                                                                      data.logradouro, '', '', data.bairro,
                                                                      data.cidade, data.estado,
                                                                      data.tipoDeLogradouro + ' ' + data.logradouro);
                $scope.buscandoCEP = false;
                $scope.classeCEP = "";
                $scope.msgCEP = "";
                $scope.cepValido = 1;
                $scope.$apply();
            }
        )
        .fail(
            function () {
                $scope.statusEndereco = 404;
                $scope.buscandoCEP = false;
                $scope.msgCEP = "CEP não corresponde a um logradouro. Preencha manualmente o endereço!!";
                //$scope.classeCEP = "has-error";
                //$scope.cepValido = 2;
                //resetEndereco();
                $scope.$apply();
            }
        )
        .always(
            function (status, text) {
                $("#overlay").hide();
            }
        );    }

    $scope.defineCategoria = function(categoria){
        $scope.participante.categoria = categoria;
        $scope.montaProduto(categoria);
    }

    $scope.verificaCPF = function() {
        cpfOk(1);
    }

    $scope.validaFormatoCPF = function() {
        cpfOk(0);
    }

    $scope.validaTelefone = function(){
        if ($scope.participante.telefone.length == 0)
            $scope.telefoneValido = 0;
        else {
            if ($scope.participante.telefone.length >= 14)
                $scope.telefoneValido = 1;
            else
                $scope.telefoneValido = 2;

        }
        return;
    }

    $scope.doPgto = function() {
        $("#overlay").show();
        if ($rootScope.sandbox)
            $state.go('app.myMeeting.inscricao.pagamento.sessionSandbox');
        else
            $state.go('app.myMeeting.inscricao.pagamento.session');
    }

    //$scope.resizeFrame = function(altura, strAltura){
    //    document.getElementById('art-main').style.height = strAltura;
    //    window.parent.iFrameHeight(altura);
    //}


    // Chamadas Iniciais

    //Auth.init();
    //Auth.toggleLogin();

    $scope.descricaoCategoria = new Array;
    $scope.descricaoCategoria[1] = 'Sócio SCEPD';
    $scope.descricaoCategoria[2] = 'Sócio SBRO';
    $scope.descricaoCategoria[3] = 'Profissionais (CDs / TPDs)';
    $scope.descricaoCategoria[4] = 'Acadêmicos';


    jsonAddr = Inscricao.jsonEndereco("", "", "", "", "", "", "", "", "");
    $scope.participante = Inscricao.jsonParticipante("", "" , "", "", "", "", jsonAddr, "", "", "", "", "", "", false, false, "");
    $scope.inscricao = Inscricao.jsonInscricao("BRL", 1, "", "");

    $scope.inscricao.currency = 'BRL'
    $scope.inscricao.itemId = 1;
    $scope.inscricao.descricao = 'Adesão ao VII Meeting da SCEPD para Sócios da SCEPD';
    $scope.inscricao.itemAmount1 = '0';

    $scope.buscandoCEP = false;
    $scope.cepValido = 0;

    $scope.cpfValido = 0;
    $scope.telefoneValido = 0;

    $scope.emailVenda = 'marcelo@scepd.meeting.net.br';

    $scope.pagamento = false;

    $scope.$watch('$viewcontentLoaded',
        function(event, viewConfig){
            user = firebase.auth().currentUser;
            if (!user) {
                 $state.go('app.signin');
            }
            else {
                $scope.participante.nome = firebase.auth().currentUser.displayName;
                $scope.participante.email = firebase.auth().currentUser.email;
            }
        }
    );

} // controller (fim)