App.controller('pagamentoImprimirBoletoCtrl',
              ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'BD', 'Auth', 'Misc', pagamentoImprimirBoletoController]);

function pagamentoImprimirBoletoController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, BD, Auth, Misc) {

    $scope.getMetodoTransacao = function(status) {
        return Misc.getMetodoTransacao(status);
    }

    $scope.getStatusTransacao = function(status) {
        return Misc.getStatusTransacao(status);
    }

    $scope.getValorTransacao = function(valor, metodo) {
        return Misc.getValorTransacao(valor, metodo);
    }

    $scope.getDataPagamento = function(data) {
        return Misc.getDataFormatada(data);
    }

}