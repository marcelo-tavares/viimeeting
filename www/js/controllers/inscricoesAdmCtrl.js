App.controller('inscricoesAdmCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', 'Inscricao', inscricoesAdmController]);

function inscricoesAdmController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc, Inscricao) {


    function montaArrayGrupos(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            obj = { nome: item.nome,
                    administrador: item.administrador,
                    fbid: i
            }
            arr.push(obj);
        });
        return arr;
    }

    function populaGrupos(snapshot){
        var arrGrupos = montaArrayGrupos(snapshot.val());
        $scope.grupos = arrGrupos;
        $scope.$apply();
    }

    function getGrupos() {
        var gruposRef = Dados.getRefDadosFilho('grupos', $rootScope.sandbox);
        return gruposRef.once('value').then(populaGrupos);
    }


    function getParticipantes() {
        var participantesRef = Dados.getRefDadosFilho('participantes', $rootScope.sandbox);
        return participantesRef.once('value').then(populaParticipantes);
    }

    function getParticipante(id) {
        var participanteRef = Dados.getRefDadosFilho('participantes/' + id, $rootScope.sandbox);
        return participanteRef.once('value').then(popupDetalhe);
    }

    function popupDetalhe(snapshot) {
        $scope.detalhe = snapshot.val();
        $scope.$apply();
        $('#modalParticipante').modal('show');
    }

    function getPagamentoParticipante(id) {
        var participanteRef = Dados.getRefDadosFilho('participantes/' + id, $rootScope.sandbox);
        return participanteRef.once('value').then(popupPagamentoDetalhe);
    }

    function popupPagamentoDetalhe(snapshot) {
        $scope.detalhe = snapshot.val();
        $scope.detalhe.fbid = snapshot.key;
        $scope.$apply();
        $('#modalPagamento').modal('show');
    }

    iconePagamento = function(metodo) {
        var arrImgCC = [
            "", "visa", "mastercard", "amex", "diners", "hipercard", "aura", "elo", "plenocard",
            "personalcard", "jcb", "discover", "brasilcard", "fortbrasil", "cardban", "valecard",
            "cabal", "mais", "avista", "grandcard"];

        var arrImgEspecie = [
            "", "dinheiro", "cheque", "grupo"];

        if (metodo != undefined) {
            if (metodo.type == "1"){
                var index = parseInt(metodo.code)-100;
                img = arrImgCC[index];
                url = "https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/" + img + ".png";
            }
            else {
                if (metodo.type == "8"){
                    var index = parseInt(metodo.code)-800;
                    img = arrImgEspecie[index];
                    url = "images/" + img + ".png";
                }
                else {
                    url = "https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/booklet.png";
                }
            }
        }
        else {
            url = "images/indefinido.png";
        }

        return url;
    }

    metodoPagamento = function (metodo) {
        if (metodo == "") {
            retorno = "images/branco.png";
        }
        else {
            retorno = iconePagamento(metodo);
        }
        return retorno;
    }

    function montaItemArray(i, item) {
        return {
            id: item.id,
            nome: item.nome,
            categoria: item.categoria,
            fbid: i
        }
    }

    function montaArray(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            if (item.id < 1000) {
                situacao = "0";
                metodoPgto = "";
                grupo = "";
                classeCssPgto="";

                if (item.grupo.nome != undefined) {
                    grupo = item.grupo.nome;
                }

                formaPgto = "9";
                if (item.categoria != 1)
                    situacao = "99";
                    if ((item.transaction != undefined) && (item.transaction != "")) {
                        situacao = item.transaction.status;
                        metodoPgto = metodoPagamento(item.transaction.paymentMethod);
                        formaPgto = item.transaction.paymentMethod.type;
                        if (situacao == 4)
                            classeCssPgto = "sucesso_claro";
                    }

                obj = { id: item.id,
                        nome: item.nome,
                        email: item.email,
                        categoria: item.categoria,
                        grupo: grupo,
                        ocultarGrupos: pertenceGrupo(grupo),
                        ocultarCortesias: recebeuCortesia(item.categoria),
                        situacao: situacao,
                        metodoPgto: metodoPgto,
                        formaPgto: formaPgto,
                        classeCssPgto: classeCssPgto,
                        fbid: i
                }
                arr.push(obj);
            }
        });
        return arr;
    }

    function pertenceGrupo(grupo) {
        retorno = "Não";
        if (grupo.length > 0)
            retorno = "Sim";
        return retorno
    }

    function recebeuCortesia(categoria) {
        retorno = "Não";
        if (categoria > 90)
            retorno = "Sim";
        return retorno
    }

    function populaParticipantes(snapshot){
        var arrParticipantes = montaArray(snapshot.val());
        $scope.participantes = arrParticipantes;
        $scope.$apply();
    }

    $scope.formataIdInscricao = function(id){
        return Misc.formataIdInscricao(id);
    }

    $scope.getGrupo = function (nomeGrupo) {
        if (nomeGrupo != undefined)
            return nomeGrupo;
        else
            return "";
    }

    $scope.getCategoria = function (id) {
        return Misc.getCategoria(id);
    }

    $scope.getComplemento = function(complemento) {
        return Misc.getComplemento(complemento)
    }

    $scope.getMetodoTransacao = function(metodo, parcelas) {
        return Misc.getMetodoTransacao(metodo, parcelas);
    }

    $scope.getStatusTransacao = function(status) {
        return Misc.getStatusTransacao(status);
    }

    $scope.getValorTransacao = function(valor, metodo) {
        return Misc.getValorTransacao(valor, metodo);
    }

    $scope.getDataPagamento = function(data) {
        return Misc.getDataFormatada(data);
    }

    $scope.getTipoInscricao = function(grupo) {
        return Misc.getTipoInscricao(grupo);
    }

    $scope.detalhes = function(id) {
        getParticipante(id);
    }

    $scope.pagamento = function(id) {
        getPagamentoParticipante(id);
    }

    $scope.grupo = function(id) {
        $scope.participanteFbid = id;
        $('#modalGrupoParticipante').modal('show');
    }

    $scope.receberEspecieCheque = function(participante) {
        $scope.participantePgto = participante;
        $('#modalPagamentoDinheiroCheque').modal('show');
    }

    $scope.toggleCampos = function () {
        $('#modalCampos').modal('show');
    }

    $scope.incluirTransacaoDinheiroCheque = function () {
        $("#overlay").show();
        var participante = $scope.participantePgto;

        id = participante.id;
        fbid = participante.fbid;
        if (participante.grupo)
            valor = Misc.buscaValorInscricaoGrupo(participante.categoria);
        else
            valor = Misc.buscaValorInscricao(participante.categoria);
        data = Date();
        metodo = $scope.metodoPagtoEspecie;

        var transacao = Inscricao.jsonTransacaoPagtoEspecie(id, fbid, valor, data, metodo)

        var dados = {};
        dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + fbid + '/transaction'] = transacao;
        firebase.database().ref().update(dados)
            .then(
                function(){
                    Misc.modalAlerta('Pagamento registrado', '', 'Sucesso!!!', 'success');
                    getParticipantes();
                    $("#overlay").hide();
                    $('#modalEditarParticipante').modal('hide');
                }
            ).catch(
                function(error) {
                    Misc.modalAlerta('Erro ao atualizar participante.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                    $("#overlay").hide();
                }
            )
    }

    $scope.editarDadosParticipante = function(participante) {
        $scope.participanteEditar = participante;
        $scope.novoNome = participante.nome;
        $scope.novaCategoria = participante.categoria.toString();
        $scope.cat = $scope.novaCategoria
        $('#modalEditarParticipante').modal('show');
    }

    $scope.podeEditarCategoria = function (situacao) {
        pode = true;
        if ((situacao > 0) && (situacao < 99))
            pode = false;
        return pode;
    }

    $scope.defineCategoria = function (categoria) {
        $scope.novaCategoria = categoria;
    }

    $scope.editarParticipante = function () {
        $("#overlay").show();
        var participante = $scope.participanteEditar;

        fbid = participante.fbid;
        categoria = $scope.novaCategoria;
        nome = $scope.novoNome;

        var novosValores = {"nome": nome, "categoria" : categoria};

        var dados = {};
        if (participante.nome != nome)
            dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + fbid + '/nome'] = nome;
        if (participante.categoria != categoria)
            dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + fbid + '/categoria'] = parseInt(categoria);
        firebase.database().ref().update(dados)
            .then(
                function(){
                    Misc.modalAlerta('Participante atualizado', '', 'Sucesso!!!', 'success');
                    getParticipantes();
                    $("#overlay").hide();
                    $('#modalEditarParticipante').modal('hide');
                }
            ).catch(
                function(error) {
                    Misc.modalAlerta('Erro ao registrar pagamento em dinheiro ou cheque.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                    $("#overlay").hide();
                }
            )
    }

    $scope.incluirGrupoParticipante = function() {
        if (!$scope.grupoParticipante){
            $("#overlay").show();
            var dados = {};
            dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + $scope.participanteFbid + '/grupo'] = "";
            firebase.database().ref().update(dados)
                .then(
                    function(){
                        Misc.modalAlerta('Participante agora não está participando de nenhum grupo.', '', 'Sucesso!!!', 'success');
                        getParticipantes();
                        $("#overlay").hide();
                        $('#modalGrupoParticipante').modal('hide');
                    }
                ).catch(
                    function(error) {
                        Misc.modalAlerta('Erro ao excluir participante de grupo.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                        $("#overlay").hide();
                    }
                )
        } else {
            $("#overlay").show();
            var nomeGrupo = $scope.grupoParticipante.nome;
            var dados = {};
            dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + $scope.participanteFbid + '/grupo'] = $scope.grupoParticipante;
            firebase.database().ref().update(dados)
                .then(
                    function(){
                        Misc.modalAlerta('Participante inserido no grupo ' + nomeGrupo, '', 'Sucesso!!!', 'success');
                        getParticipantes();
                        $("#overlay").hide();
                        $('#modalGrupoParticipante').modal('hide');
                    }
                ).catch(
                    function(error) {
                        Misc.modalAlerta('Erro ao inserir participante em grupo.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                        $("#overlay").hide();
                    }
                )
        }

    }

    $scope.toggleCortesia = function(detalhe) {
        $("#overlay").show();
        var categoria = detalhe.categoria;

        if (categoria < 90) {
            categoria = categoria + 90;
        } else {
            categoria = categoria - 90;
        }

        var dados = {};
        dados[Dados.getRoot($rootScope.sandbox) + '/participantes/' + detalhe.fbid + '/categoria'] = categoria;
        firebase.database().ref().update(dados)
            .then(
                function(){
                    Misc.modalAlerta('Alteração da cortesia concluída com sucesso', '', 'Sucesso!!!', 'success');
                    getParticipantes();
                    $("#overlay").hide();
                    $('#modalPagamento').modal('hide');
                }
            ).catch(
                function(error) {
                    Misc.modalAlerta('Erro ao alterar cortesia.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                    $('#modalPagamento').modal('hide');
                    $("#overlay").hide();
                }
            )
    }

    $scope.ocultarGrupos = function() {
        if ($scope.cbOcultarGrupos)
            $scope.filtro.ocultarGrupos = "Não";
        else
            $scope.filtro.ocultarGrupos = "";
    }

    $scope.ocultarCortesias = function() {
        if ($scope.cbOcultarCortesias)
            $scope.filtro.ocultarCortesias = "Não";
        else
            $scope.filtro.ocultarCortesias = "";
    }

    $scope.atualizaListaParticipantes = function () {
        getParticipantes();
    }

    $scope.existeTransacao = function() {
        var retorno = false;
        if ($scope.detalhe != undefined) {
            if ($scope.detalhe.transaction != undefined){
                retorno = true;
            }
        }
        return retorno;
    }

    $scope.boletoNaoPago = function() {
        var retorno = false;
        if ($scope.detalhe != undefined) {
            if ($scope.detalhe.transaction != undefined){
                //console.log($rootScope.participante);
                if (($scope.detalhe.transaction.status == "1") && ($scope.detalhe.transaction.paymentMethod.type == "2"))
                    retorno = true;
            }
        }
        return retorno;
    }

    $scope.limparFiltros = function() {
        $scope.filtro = {
            numero:"",
            nome: "",
            situacao: "",
            categoria: "",
            grupo: "",
            ocultarGrupos: "",
            ocultarCortesias: ""
        };
    }

    $scope.inicializarCampos = function () {
        $scope.campo = {
            numero: true,
            nome: true,
            email: true,
            categoria: true,
            grupo:true,
            acoes: true
        }
    }

    limparOrdenacao = function() {
        $scope.ordenacao = "id"
    }

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
      };

    $scope.propertyName = 'id';
    $scope.reverse = false;

    //Testando o git no VSCode
    $scope.teste = true;

    //Buscar e apresentar as inscrições
    getParticipantes();
    getGrupos();

    $scope.grupoParticipante = "";
    $scope.limparFiltros();
    $scope.inicializarCampos();
}



