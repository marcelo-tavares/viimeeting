App.controller('pagamentoSessionCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', pagamentoSessionController]);

function pagamentoSessionController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc) {
    sessao = document.getElementById('session');

    if (sessao) {
        $rootScope.sessionId = document.getElementById('session').value;
        PagSeguroDirectPayment.setSessionId($rootScope.sessionId);
        $state.go('app.myMeeting.inscricao.pagamento.meios');
    }
    else{
        Misc.modalAlerta('O sistema de pagamento Pagseguro está indisponível', 'Por favor tente efetuar o pagamento mais tarde', 'Ops!!!', 'danger');
        $state.go('app.myMeeting.inscricao.minhaInscricao');
    }
}