App.controller('pagamentoDadosCreditoCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', 'Inscricao', pagamentoDadosCreditoController]);

function pagamentoDadosCreditoController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc, Inscricao) {

    $scope.abreMeiosPagamento = function() {

        $state.go('app.myMeeting.inscricao.pagamento.meios')
    }

    dadosCartaoValidos = function (){
        var valida = true;
        var strMsg = '';
        var dadosCartao = $scope.dadosCartao;
        $scope.mensagensErro = "";

        $scope.classeNome = "";
        $scope.msgNome = "";
        if (dadosCartao.nome.length == 0) {
            valida = false;
            $scope.msgNome = "Nome do titular não informado";
            $scope.classeNome = "has-error";
        }

        $scope.classeCPF = "";
        $scope.msgCPF = "";
        if (dadosCartao.cpf.length == 0) {
            valida = false;
            $scope.classeCPF = "has-error";
            $scope.msgCPF = "CPF não informado";
        }

        if ($scope.cpfValido == 2)  {
            valida = false;
            $scope.classeCPF = "has-error";
            $scope.msgCPF = "CPF Inválido";
        }

        $scope.classeNumero = "";
        $scope.msgNumero = "";
        if (dadosCartao.numero.length == 0)  {
            valida = false;
            $scope.msgNumero = "Número do cartão não informado";
            $scope.classeNumero = "has-error";
        }

        $scope.classeNumero = "";
        $scope.msgNumero = "";
        if (!$scope.cardOk || dadosCartao.numero.length < 16)  {
            valida = false;
            $scope.msgNumero = "Número do cartão inválido";
            $scope.classeNumero = "has-error";
        }

        $scope.classeCVV = "";
        $scope.msgCVV = "";

        if (dadosCartao.cvv.length == 0) {
            valida = false;
            $scope.classeCVV = "has-error";
            $scope.msgCVV = "Código de segurança do cartão não informado";

        }
        else {
            tamanho = $scope.bandeira ? $scope.bandeira.brand.cvvSize : 3;
            if (dadosCartao.cvv.length < tamanho) {
                valida = false;
                $scope.classeCVV = "has-error";
                $scope.msgCVV = "Código de segurança do cartão inválido";

            }
        }

        $scope.classeTelefone = "";
        $scope.msgTelefone = "";
        if (dadosCartao.telefone.length == 0)  {
            valida = false;
            $scope.classeTelefone = "has-error";
            $scope.msgTelefone = "Telefone do titular não informado";
        }

        if ($scope.telefoneValido > 1)  {
            valida = false;
            $scope.classeTelefone = "has-error";
            $scope.msgTelefone = "Telefone do titular inválido Inválido";
        }

        $scope.classeValidade = "";
        $scope.msgMes = "";
        if (dadosCartao.mes.length == 0)  {
            valida = false;
            $scope.classeValidade = "has-error";
            $scope.msgMes = "Mês de validade não informado";
        }

        $scope.classeValidade = "";
        $scope.msgAno = "";
        if (dadosCartao.ano.length == 0)  {
            valida = false;
            $scope.classeValidade = "has-error";
            $scope.msgAno = "Ano de validade não informado";
        }

        $scope.classeNascimento = "";
        $scope.msgNascimento = "";
        if (!dadosCartao.nascimento){
            valida = false;
            $scope.msgNascimento = "Data de nascimento do titular não informada";
            $scope.classeNascimento = "has-error";
        }


        if (!valida) {
            Misc.modalAlerta('Erros no formúlário de Dados de Cartão de crédito.', 'Por favor corrija e tente novamente.', 'Ops!!!', 'danger');
        }
        return valida;
    }

    $scope.resetMsg = function(campo){
        switch (campo) {
            case 'nome':
                $scope.msgNome = "";
                $scope.classeNome = "";
                break;
            case 'cpf':
                $scope.msgCPF = "";
                $scope.classeCPF = "";
                break;
            case 'numero':
                $scope.msgNumero = "";
                $scope.classeNumero = "";
                break;
            case 'fone':
                $scope.msgTelefone = "";
                $scope.classeTelefone= "";
                break;
            case 'cvv':
                $scope.msgCVV  = "";
                $scope.classeCVV = "";
                break;
            case 'mes':
                $scope.msgMes = "";
                $scope.classeValidade = "";
                break;
            case 'ano':
                $scope.msgAno = "";
                $scope.classeValidade= "";
                break;
            case 'nascimento':
                $scope.msgNascimento = "";
                $scope.classeNascimento = "";
                break;
        }
    }

    $scope.validaTelefone = function(){
        if ($scope.dadosCartao.telefone.length == 0)
            $scope.telefoneValido = 0;
        else {
            if ($scope.dadosCartao.telefone.length >= 14)
                $scope.telefoneValido = 1;
            else
                $scope.telefoneValido = 2;
        }
        return;
    }

    cpfOk = function(){
        $scope.classeCPF = '';
        $scope.msgCPF = '';
        if ($scope.dadosCartao.cpf.length == 0)
            $scope.cpfValido = 0;
        else {
            if (Inscricao.validaCPF($scope.dadosCartao.cpf)){
                $scope.cpfValido = 1;
            }
            else
                $scope.cpfValido = 2;
        }
        return;
    }

    $scope.verificaCPF = function() {
        //
        cpfOk();
    }

    function getBrandSucesso(response) {
        $scope.bandeira = response;
        $scope.cardOk = true;
        $scope.$apply();

        PagSeguroDirectPayment.getInstallments({
            amount: Misc.buscaValorInscricao($rootScope.participante.categoria),
            maxInstallmentNoInterest: 5,
            brand: $scope.bandeira.brand.name,
            success: function (response) {
                $scope.esquemaParcelas = response.installments[$scope.bandeira.brand.name];
            },
            error: function (response) {
                // TODO: tratar erro
            },
            complete: function (response) {
                $scope.$apply();
            },
        })

    }

    function getBrandFalha(response) {
        $scope.bandeira = response;
        $scope.cardOk = false;
        $scope.$apply();
    }

    $scope.verificaBrand = function () {
        // Buscar Bandeira do Cartão
        var numero = $scope.dadosCartao.numero;
        var bandeira = "";

        if (numero.length >= 6 && !$scope.bandeira) {
            bandeira = numero.substr(0, 6);
            PagSeguroDirectPayment.getBrand({
                cardBin: bandeira,
                success: getBrandSucesso,
                error: getBrandFalha,
                complete: function(response) {}
                });
            $scope.bandeira = bandeira;
        }
        else if ($scope.dadosCartao.numero.length < 6) {
            $scope.bandeira = null;
        }

    }

    $scope.verificaParcelas = function() {
        var valorPagar = Misc.buscaValorInscricao($rootScope.participante.categoria);
        
    }

    $scope.controlaTamanhoCvv = function () {
        var tamanho = 3;
        if ($scope.bandeira != undefined)
            tamanho = $scope.bandeira.brand.cvvSize;

        if ($scope.dadosCartao.cvv.length > tamanho)
            $scope.dadosCartao.cvv = $scope.dadosCartao.cvv.substring(0,tamanho);
    }

    $scope.labelOptionParcela = function(parcelas) {
        var valorTotal = Misc.buscaValorInscricao($rootScope.participante.categoria);
        valorParcela = valorTotal / parcelas;
        return parcelas + " x R$" + Misc.formataValorMonetario(valorParcela.toFixed(2));
    }

    montaParametros = function (ccToken) {

        var valorPagar = Misc.buscaValorInscricao($rootScope.participante.categoria);

        var parcelas = $scope.dadosCartao.parcelas;

        var valorParcela = (valorPagar / parcelas).toFixed(2);

        var emailSender = $rootScope.participante.email;
        if ($rootScope.sandbox){
            emailSender = 'c55408407219930469592@sandbox.pagseguro.com.br'
        }

        var jsonPagto = {
            paymentMode:'default',
            paymentMethod:'creditCard',
            receiverEmail: 'scepd2017@gmail.com',
            currency: 'BRL',
            extraAmount: '0.00',
            itemId1: '01',
            itemDescription1: 'Meeting SCPED - Inscrição Nº' + $rootScope.participante.id,
            itemAmount1: valorPagar,
            itemQuantity1: '1',
            reference: $rootScope.idInscricaoUsuario,
            senderName: $rootScope.participante.nome,
            senderCPF: Misc.cleanCPF($rootScope.participante.cpf),
            senderAreaCode: Misc.getDDD($rootScope.participante.telefone),
            senderPhone: Misc.getFone($rootScope.participante.telefone),
            senderEmail: emailSender,
            senderHash: PagSeguroDirectPayment.getSenderHash(),
            shippingAddressStreet: $rootScope.participante.endereco.logradouroCompleto,
            shippingAddressNumber: $rootScope.participante.endereco.numero,
            shippingAddressComplement: $rootScope.participante.endereco.complemento,
            shippingAddressDistrict: $rootScope.participante.endereco.bairro,
            shippingAddressPostalCode: $rootScope.participante.endereco.cep,
            shippingAddressCity: $rootScope.participante.endereco.cidade,
            shippingAddressState: $rootScope.participante.endereco.estado,
            shippingAddressCountry: 'BRA',
            shippingType: '3',
            shippingCost: '0.00',
            creditCardToken: ccToken,
            installmentQuantity: $scope.esquemaParcelas[parcelas-1].quantity,
            installmentValue: $scope.esquemaParcelas[parcelas-1].installmentAmount.toFixed(2),
            noInterestInstallmentQuantity: 5,
            creditCardHolderName : $scope.dadosCartao.nome,
            creditCardHolderCPF : Misc.cleanCPF($scope.dadosCartao.cpf),
            creditCardHolderBirthDate : Misc.getDataFormatada($scope.dadosCartao.nascimento),
            creditCardHolderAreaCode : Misc.getDDD($scope.dadosCartao.telefone),
            creditCardHolderPhone : Misc.getFone($scope.dadosCartao.telefone),
            billingAddressStreet: $rootScope.participante.endereco.logradouroCompleto,
            billingAddressNumber: $rootScope.participante.endereco.numero,
            billingAddressComplement: $rootScope.participante.endereco.complemento,
            billingAddressDistrict: $rootScope.participante.endereco.bairro,
            billingAddressPostalCode: $rootScope.participante.endereco.cep,
            billingAddressCity: $rootScope.participante.endereco.cidade,
            billingAddressState: $rootScope.participante.endereco.estado,
            billingAddressCountry: 'BRA'
        }
        return jsonPagto;
    } /* montaParametros */

    enviaEmail = function(para, assunto, mensagem){
        dadosEmail = {
            nomeRemetente: 'Secretaria do VII Meeting da SCEPD',
            emailRemetente: 'secretaria@scepd.meeting.net.br',
            emailDestinatario: para,
            emailCc: '',
            emailCco: '',
            assunto: assunto,
            mensagem: mensagem
        }

        Misc.enviaEmail(dadosEmail);
    }

    atualizarTransacaoOk = function (erro) {
        if (erro) {
            Misc.modalAlerta('Erro ao gravar transação', 'Tente efetuar novamente pelo botão na sessão Dados de Pagamento de sua Inscrição!', 'Ops!!!', 'danger');
        }
        else
        {
            Misc.modalAlerta('Transação gravada com sucesso!', 'Imprima seu boleto pelo botão na sessão Dados de Pagamento de sua Inscrição!', 'Ok!!!', 'success');

            $state.go('app.myMeeting.inscricao.minhaInscricao');
        }
        $("#overlay").hide();
    }

    atualizarTransacao = function (transacao) {
        enviaEmail("marcelo@tavares.eti.br", "Transação Pagseguro", transacao);
        var ref = Dados.getRefDadosFilho('participantes/' + $rootScope.idInscricaoUsuario, $rootScope.sandbox)
        ref.update(transacao, atualizarTransacaoOk);
    }

   function doPagtoSucesso(response) {
        json = Misc.xml2Json($.parseXML(response));

        // Retirar #Text do json
        json = Misc.LimpaText(json)

        if (json.transaction){
            // Gravar transação no BD
            atualizarTransacao(json);
        } else {
            // abrir modal de mensagem de Erro
            Misc.modalAlerta('Erro ao gravar transação', 'Tente efetuar novamente pelo botão na sessão Dados de Pagamento de sua Inscrição!', 'Ops!!!', 'danger');
            $("#overlay").hide();
        }
    }

   function doPagtoFalha(jqxhr) {
        // obtendo lista de erros
        //var response = $.parseJSON(jqxhr.responseText);

        // Exibindo lista de erros
        //showPaymentErrors(response.errors);

            Misc.modalAlerta('Erro ao processar a transação', '', 'Ops!!!', 'danger');
            $("#overlay").hide();
   }


    doPagtoCartaoCredito = function (ccToken) {
        $("#overlay").show();
        var url = 'pagamento/pagamento.php'
        if ($rootScope.sandbox){
            url = 'pagamento/pagamento_sandbox.php'
        }
        var params = montaParametros(ccToken);
        var ajaxConfig = {
            type:'POST',
            url: url,
            data: params,
            cache: false,
            success: doPagtoSucesso,
            error: doPagtoFalha
        }
        $.ajax(ajaxConfig);
    } /* doPagtoCartaoCredito */

    createCardTokenSucesso = function(response) {
        var ccToken = response.card.token;
        doPagtoCartaoCredito(ccToken);
    }

    createCardTokenFalha = function(response) {
        var strErros = Misc.listaErros(response.errors);
        Misc.modalAlerta('Erro ao processar dados do Cartão de Crédito.', strErros, 'Ops!!!', 'danger');
    }

    $scope.enviarPagamento = function () {
        if (dadosCartaoValidos()) {
            var params = {
                cardNumber: $scope.dadosCartao.numero,
                brand: $scope.bandeira.brand.name,
                cvv: $scope.dadosCartao.cvv,
                expirationMonth: $scope.dadosCartao.mes,
                expirationYear: $scope.dadosCartao.ano,
                success: createCardTokenSucesso,
                error: createCardTokenFalha,
                complete: function(response) {}
            }

            PagSeguroDirectPayment.createCardToken(params);
        }
    }


    // Inicializações
    $scope.cpfValido = 0;
    $scope.telefoneValido = 0;
    $scope.dadosCartao = Inscricao.jsonCartaoCredito("", "", "", "", "", "", "", "", "1");
    $scope.cardOk = false;
    $scope.bandeira = "";

    //$scope.urlImg = "meeting_net_br/scepd/system/images/";
    $scope.urlImg = "images/";
    if ($rootScope.sandbox)
        $scope.urlImg = "images/";

}
