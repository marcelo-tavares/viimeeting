App.controller('perfilCtrl', ['$scope', '$rootScope', '$state', '$firebase', 'Dados', perfilController]);

function perfilController ($scope, $rootScope, $state, $firebase, Dados) {
    $rootScope.pagamento = false;

    $scope.queroAlterarMinhaSenha = function(){
        fechaMsg();
        firebase.auth().sendPasswordResetEmail(firebase.auth().currentUser.email)
            .then(
                function() {
                    $scope.flagSucesso = true;
                    $scope.msgSucesso = '<strong>Ok!</strong> Enviamos uma mensagem com link para o e-mail cadastrado. Clique nele para informar uma nova senha.';
                    $scope.$apply();
                }
            )
            .catch(
                function(error) {
                    $scope.flagErro = true;
                    $scope.msgErro = '<strong>Ops!</strong> Um erro ocorreu e não foi possível concluir a operação. Tente mais tarde.';
                    $scope.$apply();
                }
            );
    }
}
