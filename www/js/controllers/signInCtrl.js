App
.controller('signInCtrl', ['$scope', '$rootScope', '$state', '$firebase', 'Dados', 'Auth', 'Misc', 'ngDialog', signInController]);

function signInController($scope, $rootScope,$state, $firebase, Dados, Auth, Misc, ngDialog){

    $scope.abrirSignUp = function() {
        $state.go('app.signup')
    }

    $scope.myMeeting = function() {
        $state.go('app.myMeeting.perfil');
    }

    $scope.validaEmail = function() {
        if ($scope.emailValido == 0) {
            $scope.msgEmail = "O campo E-mail deve ser informado.";
            $scope.classeEmail = "has-error";
        }
        else
            if ($scope.emailValido == 2) {
                $scope.msgEmail = "O formato do E-mail é inválido.";
                $scope.classeEmail = "has-error";
            } else {
                $scope.msgEmail = "";
                $scope.classeEmail = "";
            }
    }

    $scope.validaFormatoEmail = function() {
        var emailPattern =  /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/;
        if ($scope.inputUser.length == 0)
            $scope.emailValido = 0;
        else
            if (Misc.validaExpReg($scope.inputUser, emailPattern)){
                $scope.emailValido = 1;
            }
            else {
                $scope.emailValido = 2;
            }
        return;
    }

    $scope.validaFormatoSenha = function() {
        if ($scope.inputPassword.length == 0)
            $scope.senhaValida = 0;
        else
            if ($scope.inputPassword.length >= 8){
                $scope.senhaValida = 1;
            }
            else {
                $scope.senhaValida = 2;
            }
        return;
    }

    $scope.validaSenha = function() {
        if ($scope.inputPassword.length == 0){
            $scope.msgSenha = "O campo Senha deve ser informada.";
            $scope.classeSenha = "has-error";
        }
        else {
            if ($scope.inputPassword.length < 8){
                $scope.msgSenha = "O campo Senha deve ter pelo menos 8 caracteres.";
                $scope.classeSenha = "has-error";
            }
            else {
                $scope.msgSenha = "";
                $scope.classeSenha = "";
            }
        }

    }

    $scope.fecharMsgEmail = function () {
        $scope.msgEmail = "";
        $scope.classeEmail = "";
    }

    $scope.fecharMsgSenha = function () {
        $scope.msgSenha = "";
        $scope.classeSenha = "";
    }

    fechaMsg = function () {
        $scope.flagErro = false;
        $scope.flagSucesso = false;
    }



    $scope.autenticarUsuario = function (){
        fechaMsg();
        $("#overlay").show();
        firebase.auth().signInWithEmailAndPassword($scope.inputUser, $scope.inputPassword)
            .then(
                function () {
                    $scope.flagSucesso = true;
                    $scope.msgSucesso = '<strong>Ok!</strong> usuário autenticado';
                    $scope.$apply();

                    $state.go('app.myMeeting.perfil');
                }
            )
            .catch(
                function(error) {
                    $scope.flagErro = true;
                    $scope.msgErro = Auth.mensagemErroLogin(error);
                    $scope.$apply();
                }
            );
    }

    $scope.esqueciMinhaSenha = function(){
        fechaMsg();

        if ($scope.inputUser == ''){
            $scope.flagErro = true;
            $scope.msgErro = '<strong>Ops!</strong> Informe o e-mail do seu usuário cadastrado.';
        }
        else {
            $("#overlay").show();
            firebase.auth().sendPasswordResetEmail($scope.inputUser)
                .then(
                    function() {
                        $scope.flagSucesso = true;
                        $scope.msgSucesso = '<strong>Ok!</strong> Enviamos uma mensagem com link para o e-mail informado. Clique nele para informar uma nova senha.';
                        $scope.$apply();
                        $("#overlay").hide();
                    }
                )
                .catch(
                    function(error) {
                        if (error.code == 'auth/invalid-email') {
                            $scope.flagErro = true;
                            $scope.msgErro = '<strong>Ops!</strong> O e-mail informado é inválido.';
                        } else
                            if (error.code == 'auth/user-not-found') {
                                $scope.flagErro = true;
                                $scope.msgErro = '<strong>Ops!</strong> Não encontramos o usuário com o e-mail informado.';
                            }
                        $scope.$apply();
                    }
                );
        }
    }


    if (firebase.auth().currentUser)
        $state.go('app.myMeeting.perfil');

    // ------------------------
    $scope.emailValido = 0;
    $scope.senhaValida = 0;
    $scope.inputPassword = '';
    $scope.inputUser = '';
}
