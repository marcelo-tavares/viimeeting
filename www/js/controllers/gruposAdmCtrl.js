App.controller('gruposAdmCtrl', ['$scope', '$rootScope', '$state', '$sce', '$http', 'Misc', 'Inscricao', 'Dados', 'BD', 'Auth', gruposAdmController]);

function gruposAdmController($scope, $rootScope, $state, $sce, $http, Misc, Inscricao, Dados, BD, Auth){

    $scope.limparFiltros = function() {
        $scope.filtro = {
            nomeGrupo:""
        };
    }

    limparOrdenacao = function() {
        $scope.ordenacao = "nomeGrupo"
    }


    function getParticipantes() {
            var participantesRef = Dados.getRefDadosFilho('participantes', $rootScope.sandbox);
            return participantesRef.once('value').then(populaParticipantes);
        }

   function montaArrayParticipantes(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            obj = { id: item.id,
                    nome: item.nome,
                    fbid: i
            }
            arr.push(obj);
        });
        return arr;
    }

   function montaArrayGrupos(data) {
        var arr = Array();
        var obj = {};

        $.each(data, function(i, item) {
            obj = { nome: item.nome,
                    administrador: item.administrador,
                    fbid: i
            }
            arr.push(obj);
        });
        return arr;
    }

    function populaParticipantes(snapshot){
        var arrParticipantes = montaArrayParticipantes(snapshot.val());
        $scope.participantes = arrParticipantes;
        $scope.$apply();
    }

    function populaGrupos(snapshot){
        var arrGrupos = montaArrayGrupos(snapshot.val());
        $scope.grupos = arrGrupos;
        $scope.$apply();
    }

    function getGrupos() {
        var gruposRef = Dados.getRefDadosFilho('grupos', $rootScope.sandbox);
        return gruposRef.once('value').then(populaGrupos);
    }

    function getGrupo(id) {
        var grupoRef = Dados.getRefDadosFilho('grupos/' + id, $rootScope.sandbox);
        return grupoRef.once('value').then(popupDetalhe);
    }

    function popupDetalhe(snapshot) {
        $scope.detalhe = snapshot.val();
        $scope.$apply();
        $('#modalGrupo').modal('show');
    }

    $scope.criarNovoGrupo = function() {
        $('#modalGrupo').modal('show');
    }

    $scope.incluirGrupo = function() {
        if ($scope.grupo.nome.length == 0) {
            Misc.modalAlerta('Nome do grupo não informado.', 'Por favor corrija e tente novamente.', 'Ops!!!', 'danger');
        } else
            if ($scope.grupo.administrador.length == 0) {
                Misc.modalAlerta('Administrador do grupo não selecionado.', 'Por favor corrija e tente novamente.', 'Ops!!!', 'danger');
            } else {
                // Inclusão do Grupo
                $("#overlay").show();
                var grupoRef = Dados.getRefDados();
                var newNodeKey = grupoRef.child('grupos').push().key;
                var dados = {};
                dados[Dados.getRoot($rootScope.sandbox) + '/grupos/' + newNodeKey] = $scope.grupo;
                firebase.database().ref().update(dados)
                    .then(
                        function(){
                            $scope.hashGrupo = newNodeKey;
                            getGrupos();
                            $scope.$apply();
                            $("#overlay").hide();
                            $('#modalGrupo').modal('hide');
                        }
                    ).catch(
                        function(error) {
                             Misc.modalAlerta('Erro ao gravar grupo.', 'Tente novamente mais tarde.', 'Ops!!!', 'danger');
                             $("#overlay").hide();
                        }
                    )
            }
    }

    getParticipantes();
    getGrupos();
    $scope.grupo = Inscricao.jsonGrupo("", "", "");
}