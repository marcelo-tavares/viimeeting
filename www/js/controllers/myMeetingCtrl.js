App.controller('myMeetingCtrl', ['$scope', '$rootScope', '$state', '$firebase', 'Dados', 'Auth', 'Misc', myMeetingController]);

function myMeetingController($scope, $rootScope, $state, $firebase, Dados, Auth, Misc) {

    $scope.abreMeuPerfil = function() {
        $state.go('app.myMeeting.perfil')
    }

    $scope.abreMinhaInscricao = function() {
            $state.go('app.myMeeting.inscricoes')
        }

    $scope.doLogout = function() {
        if (firebase.auth().currentUser) {
            firebase.auth().signOut()
                .then(
                    function() {
                        $rootScope.adminUser = false;
                        $state.go('app.signin');
                    },
                    function(error) {
                        alert('Erro ao desconectar usuário (' + error.message + ')')
                    });
        }
    }

    getPrimeiroNome = function(nome) {
        try {
            primeiroNome = nome.substr(0, nome.indexOf(" "));
        }
        catch (err) {
            primeiroNome = "???";
            console.log(err);
        }
        finally {
            return primeiroNome;
        }
    }

    buscarInscricaoUsuarioCorrente = function() {
        var retorno = null;

        if (firebase.auth().currentUser) {
            var uid = firebase.auth().currentUser.uid;
            var ref = firebase.database().ref(Dados.getRoot($rootScope.sandbox) + '/participantes');
            $scope.resultadoBuscaOk = false;
            ref.orderByChild('uid').equalTo(uid).once('value')
                .then(
                    function(snapshot) {
                        if (!snapshot.hasChildren()) {
                            $rootScope.participante = null
                            $rootScope.$apply();
                            $("#overlay").hide();
                        }
                        else {
                            snapshot.forEach(
                                function(childSnapshot) {
                                    key = childSnapshot.key;
                                    participante = childSnapshot.val();

                                    $rootScope.idInscricaoUsuario = key;
                                    $rootScope.participante = participante;
                                    $rootScope.$apply();

                                    $("#overlay").hide();
                                }
                            );
                        }
                    }
                )
                .catch(
                    function (error) {
                        $rootScope.participante = null
                        $rootScope.$apply();
                        $("#overlay").hide();
                    }
                );
        }
    }


    administrador = function() {
        var uid = firebase.auth().currentUser.uid;
        var ref = firebase.database().ref(Dados.getRoot($rootScope.sandbox) + '/config/administradores/' + uid);
        $scope.admin = false;

        ref.once('value', function (snapshot) {
            if (snapshot.val()){
                $rootScope.adminUser = true;
                $rootScope.$apply();
            }
        });
    }


    // Se não estiver autenticado, ir para a tela de login
    if (!firebase.auth().currentUser) {
        $state.go('app.signin');
    }
    else {
        $scope.usuario = Auth.jsonUsuario(
            firebase.auth().currentUser.displayName,
            firebase.auth().currentUser.email,
            getPrimeiroNome(firebase.auth().currentUser.displayName));

        administrador();
        buscarInscricaoUsuarioCorrente();
        $("#overlay").hide();
    }
}

