App.controller('pagamentoMeiosCtrl', ['$scope', '$rootScope', '$state', '$firebase', '$firebaseArray', 'Dados', 'Misc', 'Config', pagamentoMeiosController]);

function pagamentoMeiosController ($scope, $rootScope, $state, $firebase, $firebaseArray, Dados, Misc, Config) {

    $("#overlay").show();
    PagSeguroDirectPayment.getPaymentMethods({
        amount: 500.00,
        success: function (response) {
            $scope.meiosPgto = response.paymentMethods;
            $scope.boleto = $scope.meiosPgto['BOLETO'];
            $scope.credito = $scope.meiosPgto['CREDIT_CARD'];
            $scope.debito = $scope.meiosPgto['ONLINE_DEBIT'];

            $scope.$apply();
        },
        error: function (response) {
            console.log(response.errors);
            $scope.meiosPgto = 'Erro ao buscar meios de pagamento';
            $scope.$apply();
        },
        complete: function (response) {
            $("#overlay").hide();
        }
    });

    // Valor da Inscrição
    $scope.valorInscricao = Misc.buscaValorInscricao($rootScope.participante.categoria).replace(".", ",");

    $scope.abreMinhaInscricao = function() {
        $state.go('app.myMeeting.inscricao.minhaInscricao')
    }

    $scope.abreDadosPagamento = function(pagamentoMeio) {
        switch (pagamentoMeio) {
            case 0:
                $scope.abreDadosBB();
                break;
            case 1:
                $scope.abreDadosCC();
                break;
            case 2:
                $scope.abreDadosCD();
                break;
            default:
                Misc.modalAlerta('Você precisa escolher um meio de pagamento.', '', 'Ops!!!', 'danger');
                break;
        }
    }

    $scope.abreDadosBB = function() {
        $state.go('app.myMeeting.inscricao.pagamento.dadosBoleto')
    }

    $scope.abreDadosCC = function() {
        $state.go('app.myMeeting.inscricao.pagamento.dadosCredito')
    }

    $scope.abreDadosCD = function() {
        $state.go('app.myMeeting.inscricao.pagamento.dadosDebito')
    }
}
