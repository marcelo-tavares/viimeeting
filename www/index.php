<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head><!-- Created by Artisteer v4.3.0.60858 -->
        <meta charset="utf-8">
        <title>VII Meeting da SCEPD - Inscrições</title>
        <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

        <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="style.css" media="screen">
        <!--[if lte IE 7]><link rel="stylesheet" href="style.ie7.css" media="screen" /><![endif]-->
        <link rel="stylesheet" href="style.responsive.css" media="all">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin">
        
        <style>
            .art-content .art-postcontent-0 .layout-item-0 { padding-right: 10px;padding-left: 10px;  }
            .ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
            .ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
        </style>

        <!-- template scripts -->
        <script src="jquery.js"></script>
        <script src="script.js"></script>
        <script src="script.responsive.js"></script>

        <!-- Latest compiled and minified CSS -->
        <script src="https://www.gstatic.com/firebasejs/ui/live/0.5/firebase-ui-auth.js"></script>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="lib/css/spinner.css" />
        <link type="text/css" rel="stylesheet" href="lib/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="lib/css/estilos.css" />
        <link type="text/css" rel="stylesheet" href="lib/css/overlay.css" />
</head>
    
    <body  ng-app="appInscricoes">
        <div id="art-main">
            <header class="art-header">
                <div class="art-shapes">
                    <div class="art-object1577736972"></div>
                    <div class="art-object667786516"></div>
                    <div class="art-object1883278182"></div>
                    <div class="art-object1211978409"></div>
                    <div class="art-object1790035276"></div>
                    <div class="art-object675360060"></div>
                    <div class="art-object1380345593"></div>
                    <div class="art-object1287289463"></div>
                    <div class="art-object1548336044"></div>
                    <div class="art-object774691281"></div>
                    <div class="art-object781181879"></div>
                    <div class="art-object1544567456"></div>
                </div>

                <nav class="art-nav">
                    <div class="art-nav-inner">
                        <ul class="art-hmenu">
                            <li class="item-101"><a href="http://www.scepd.meeting.net.br/index.php">Home</a></li>
                            <li class="item-110"><a href="http://www.scepd.meeting.net.br/index.php/local-do-evento">Local do Evento</a></li>
                            <li class="item-111"><a href="http://www.scepd.meeting.net.br/index.php/palestrantes">Palestrantes</a></li>
                            <li class="item-118"><a href="http://www.scepd.meeting.net.br/index.php/hospedagem">Hospedagem</a></li>
                            <li class="item-132"><a href="http://www.scepd.meeting.net.br/index.php/fale-conosco">Fale Conosco</a></li>
                            <li class="item-135"><a href="http://www.scepd.meeting.net.br/index.php/inscricaoonline">Inscrições</a></li>
                        </ul>
                    </div>
                </nav>
            </header>

            <div class="art-sheet clearfix">
                <div class="art-layout-wrapper">
                    <div class="art-content-layout">
                        <div class="art-content-layout-row">
                            <h2 class="h2-mee">Sistema de Inscrições</h2>
                            <hr>
                            <div ui-view>Carregando...</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <iframe style="width:0px; height:0px;" id="se" src="email/index.php"></iframe>

            <footer class="art-footer">
                <div class="art-footer-inner">
                </div>
            </footer>

            <!-- lib scripts -->
            <script src="lib/js/angular.js"></script>
            <script src="lib/js/angular-ui-router.js"></script>
            <script src="lib/js/jquerymask.js"></script>
            <script src="lib/js/angular-sanitize.min.js"></script>
            <script src="lib/js/heartcode-canvasloader.js"></script>
            <script src="lib/js/ngDialog.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="lib/js/ui-bootstrap-tpls-2.2.0.min.js"></script>
            <script src="lib/js/xmlToJson.min.js"></script>

            <!-- Firebase, Firebase UI Auth e AngularFire  -->
            <script src="https://www.gstatic.com/firebasejs/3.2.1/firebase.js"></script>
            <script src="https://cdn.firebase.com/libs/angularfire/1.2.0/angularfire.min.js"></script>
            <script src="https://www.gstatic.com/firebasejs/ui/live/0.4/firebase-ui-auth.js"></script>

            <!-- Pagseguro Sandbox -->
            <script src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js" type="text/javascript"></script>

            <!-- Pagseguro Production
            <script src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js" type="text/javascript"></script>
            -->

            <!-- local scrips -->
            <script src="js/app.js"></script>

            <!-- Factories -->
            <script src="js/factories/authFactory.js"></script>
            <script src="js/factories/bdFactory.js"></script>
            <script src="js/factories/inscricaoFactory.js"></script>
            <script src="js/factories/miscFactory.js"></script>

            <script src="js/services.js"></script>
            <script src="js/directives.js"></script>
            <script src="js/values.js"></script>

            <!-- Controllers -->
            <script src="js/controllers/appCtrl.js"></script>
            <script src="js/controllers/individualCtrl.js"></script>
            <script src="js/controllers/gruposAdmCtrl.js"></script>
            <script src="js/controllers/signUpCtrl.js"></script>
            <script src="js/controllers/signInCtrl.js"></script>
            <script src="js/controllers/inscricaoCtrl.js"></script>
            <script src="js/controllers/minhaInscricaoCtrl.js"></script>
            <script src="js/controllers/meusGruposCtrl.js"></script>
            <script src="js/controllers/inscricoesAdmCtrl.js"></script>
            <script src="js/controllers/perfilCtrl.js"></script>
            <script src="js/controllers/myMeetingCtrl.js"></script>
            <script src="js/controllers/pagamentoSessionCtrl.js"></script>
            <script src="js/controllers/pagamentoCtrl.js"></script>
            <script src="js/controllers/pagamentoMeiosCtrl.js"></script>
            <script src="js/controllers/pagamentoDadosBoletoCtrl.js"></script>
            <script src="js/controllers/pagamentoImprimirBoletoCtrl.js"></script>
            <script src="js/controllers/pagamentoDadosCreditoCtrl.js"></script>
            <script src="js/controllers/pagamentoDadosDebitoCtrl.js"></script>
            <script src="js/controllers/dadosParticipantesCtrl.js"></script>

            <script src="js/init.js"></script>
        </div>
    </body>
</html>