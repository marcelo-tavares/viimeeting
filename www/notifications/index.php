<?php
/*
 ************************************************************************
 Copyright [2014] [PagSeguro Internet Ltda.]
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************
 */
header("Access-Control-Allow-Origin: *");

require_once "./PagSeguroLibrary/PagSeguroLibrary.php";
require_once "./lib/HttpConnection.class.php";

class NotificationListener
{
    public static function main()
    {
        //self::enviaEmail("Entrou no processamento da notificação");


        $code = (isset($_POST['notificationCode']) && trim($_POST['notificationCode']) !== "" ?
            trim($_POST['notificationCode']) : null);
        $type = (isset($_POST['notificationType']) && trim($_POST['notificationType']) !== "" ?
            trim($_POST['notificationType']) : null);

        if ($code && $type) {
            $notificationType = new PagSeguroNotificationType($type);
            $strType = $notificationType->getTypeFromValue();
            switch ($strType) {
                case 'TRANSACTION':
                    self::transactionNotification($code);
                    break;
                case 'APPLICATION_AUTHORIZATION':
                    self::authorizationNotification($code);
                    break;
                case 'PRE_APPROVAL':
                    self::preApprovalNotification($code);
                    break;
                default:
                    LogPagSeguro::error("Unknown notification type [" . $notificationType->getValue() . "]");
            }
            //self::printLog($strType);
        } else {
            LogPagSeguro::error("Invalid notification parameters.");
            self::printLog();
        }
    }

    private static function transactionNotification($notificationCode) {
        $credentials = PagSeguroConfig::getAccountCredentials();
        try {
            $transaction = PagSeguroNotificationService::checkTransaction($credentials, $notificationCode);

            // Do something with $transaction
            self::atualizaStatus($transaction);
           // **********************************************************************

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public static function atualizaStatus($transaction) {
        $referencia = $transaction->getReference();
        $sender = $transaction->getSender();

        $st = $transaction->getStatus();
        $status = $st->getValue();

        $dados = "dados";
        $secret = "CFL3yqSeZiyXaCDoBYKlmK1MErb1VWkhYUmJ3a0a";
        $url = "https://vii-meeting-scepd-dbac7.firebaseio.com/$dados/participantes/$referencia/transaction/.json?auth=$secret";
        $jsonParams = '{"status":"'.$status.'"}';

        $httpConnection = new HttpConnection();
        $httpConnection->patch($url, $jsonParams);
        $resposta = $httpConnection->getResponse();

        self::enviaEmail($sender->$name.": ".$resposta);
    }


    private static function authorizationNotification($notificationCode)
    {
        $credentials = PagSeguroConfig::getApplicationCredentials();
        try {
            $authorization = PagSeguroNotificationService::checkAuthorization($credentials, $notificationCode);
            // Do something with $authorization
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    private static function preApprovalNotification($preApprovalCode)
    {
        $credentials = PagSeguroConfig::getAccountCredentials();
        try {
            $preApproval = PagSeguroNotificationService::checkPreApproval($credentials, $preApprovalCode);
            // Do something with $preApproval

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    private static function enviaEmail($mensagem) {
        // Preparando o e-mail para debug
        $quebra_linha = "\n";

        $nomeremetente     = "Secretaria do VII Meeting da SCEPD";
        $emailremetente    = "secretaria@scepd.meeting.net.br";
        $emaildestinatario = "marcelotavares1965@gmail.com";
        $assunto           = "Processamento de Notificação";
        $emailsender       = "webmaster@scepd.meeting.net.br";

        /* Montando a mensagem a ser enviada no corpo do e-mail. */
        $mensagemHTML = $mensagem;


        /* Montando o cabeçalho da mensagem */
        $headers = "MIME-Version: 1.1".$quebra_linha;
        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
        // Perceba que a linha acima contém "text/html", sem essa linha, a mensagem não chegará formatada.
        $headers .= "From: ".$emailsender.$quebra_linha;
        $headers .= "Return-Path: " . $emailsender . $quebra_linha;
        $headers .= "Reply-To: ".$emailremetente.$quebra_linha;
        // Note que o e-mail do remetente será usado no campo Reply-To (Responder Para)

        /* Enviando a mensagem */
        mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);
        //mail($emaildestinatario, $assunto, $mensagemHTML, $headers);
    }

    private static function printLog($strType = null)
    {
        $count = 4;
        echo "<h2>Receive notifications</h2>";
        if ($strType) {
            echo "<h4>notificationType: $strType</h4>";
        }
        echo "<p>Last <strong>$count</strong> items in <strong>log file:</strong></p><hr>";
        echo LogPagSeguro::getHtml($count);
    }
}
NotificationListener::main();